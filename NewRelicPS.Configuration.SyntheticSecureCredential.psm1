<#
.Synopsis
  Idempotently applies New Relic synthetic secure credential configurations
.Description
  Idempotently applies New Relic synthetic secure credential configurations.
.Example
  Set-SyntheticSecureCredential -AdminAPIKey $AdminAPIKey -DefinedSytheticSecureCredentials $Config.SyntheticSecureCredentials
  Uses New Relic APIs to update synthetic secure credentials to match the configuration defined in $Config.SyntheticSecureCredentials.  Any existing secure credentials that are not defined will be removed.
.Parameter AdminAPIAPIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter DefinedSyntheticSecureCredentials
  An array of secure credential objects which define the desired configuration state for New Relic synthetic secure credentials.
#>
Function Set-NRSyntheticSecureCredentialConfiguration {
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSShouldProcess", '', Justification = "All CMDLets being called have ShouldProcess in place and the preference is passed to them." )]
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AdminAPIKey,
    [array] $DefinedSyntheticSecureCredentials
  )

  # Iterate over each defined secure credential
  [System.Collections.ArrayList]$existingSecureCredentials = @(Get-NRSyntheticSecureCredential -APIKey $AdminAPIKey)
  Write-Verbose "There are currently $($DefinedSyntheticSecureCredentials.count) defined Synthetic secure credentials and $($existingSecureCredentials.key.count) existing Synthetic secure credentials."
  Foreach ($secureCredential in $DefinedSyntheticSecureCredentials) {
    $existingSecureCredential = $existingSecureCredentials | Where-Object {$_.key -eq $secureCredential.key}
    $Params = @{
      APIKey = $AdminAPIKey
      Key = $secureCredential.Key.TOUPPER()
      Value = $secureCredential.Value
      Description = $secureCredential.Description
    }

    If ($existingSecureCredential) {
      # The secure credentials API does not return the secret value so there is no way to check it.  Therefore, update existing keys on each run.
      Write-Verbose "Updating secure credential $($secureCredential.key.ToUpper())"
      Update-NRSyntheticSecureCredential @Params -Whatif:$WhatIfPreference | Out-Null

      # Any extra existing secure credentials that are not defined will be removed later
      $existingSecureCredentials.Remove($existingSecureCredential)
    }

    # Otherwise, create secure credential
    Else {
      Write-Verbose "Creating secure credential $($secureCredential.Key.ToUpper())"
      New-NRSyntheticSecureCredential @Params -Whatif:$WhatIfPreference | Out-Null
    }
  }

  # Check for existing secure credentials not in the definition and remove them
  # Note that the Synthetic API returns an object with empty properties rather than null when no secure credentials exist so check using the key property
  If ($existingSecureCredentials.key) {
    Write-Verbose "Removing secure credential(s) $($existingSecureCredentials.Key.ToUpper() -join (','))"
    $existingSecureCredentials.Key | Remove-NRSyntheticSecureCredential $AdminAPIKey  -Whatif:$WhatIfPreference | Out-Null
  }
}