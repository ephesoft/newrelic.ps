<#
.Synopsis
  Gets New Relic Synthetic Credentials
.Description
  Gets all New Relic synthetic secure credentials in an account or returns a specific credential if a credential key is provided.
.Example
  Get-NRSyntheticSecureCredential -APIKey $APIKey
  Gets information on all New Relic synthetic secure credentials in the account associated with the API key provided.
.Example
  Get-NRSyntheticSecureCredential -APIKey $APIKey -Key 123
  Returns only data for the secure credential with key 123 in the account associated with the API key provided.  Returns null if no existing credential has the key provided.
.Parameter APIKey
  This must be an Admin API key. See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Key
  Retrieves only the credential with the provided key.  Returns null if a credential with the specified key is not found.
#>

Function Get-NRSyntheticSecureCredential {
  [CMDLetBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [string] $Key
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = 'https://synthetics.newrelic.com/synthetics/api/v1/secure-credentials'
    If ($Key) {
      $url += "/$Key"
    }

    # Call the API and return null if a credential key is provided but not found
    try {
      $result = Invoke-RestMethod -Uri $url -Headers $headers -Method 'Get' -ContentType 'application/json'
    }
    catch {
      If ($_.Exception.Response.StatusCode.Value__ -ne '404') {
        Write-Error $_
      }
    }

    # The result is wrapped in the securecredentials property unless a single result is returned so use ternary to return consistent results
    $result.securecredentials ? $result.securecredentials : $result
  }
}

<#
.Synopsis
  Creates a New Relic Synthetic Secure Credential
.Description
  Creates a New Relic synthetic secure credential.
.Example
  New-NRSyntheticSecureCredential -APIKey $APIKey -Key 'MyCredential1' -Value '1234' -Description 'This is a weak credential'
  Creates a new Synthetics secure credential.
.Parameter APIKey
  This must be an Admin API key. See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Key
  The non-secure label used to identify the credential.
.Parameter Value
  The secure value to store in the credential.
.Parameter Description
  User-friendly text used to describe the credential.
#>

Function New-NRSyntheticSecureCredential {
  [CMDLetBinding(SupportsShouldProcess=$true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Key,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Value,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [string] $Description = $true
  )
  Begin {
    $url = 'https://synthetics.newrelic.com/synthetics/api/v1/secure-credentials'
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    # Build up the body of the request
    $Body = @{
      key = $Key.ToUpper()
      value = $Value
      description = $Description
    }

    # Call the API
    If ($PSCmdlet.ShouldProcess($Key, "Create synthetic secure credential")) {
      Invoke-RestMethod -Uri $url -Headers $headers -Body ($Body | ConvertTo-Json) -Method 'Post' -ContentType 'application/json'
    }
  }
}

<#
.Synopsis
  Deletes a New Relic Synthetic Secure Credential
.Description
  Deletes a New Relic synthetic secure credential.
.Example
  Remove-NRSyntheticSecureCredential -APIKey $APIKey -Key 'MyCredential'
  Deletes the synthetic secure credential with key MyCredential from the account associated with the provided API key
.Example
  $Keys | Remove-NRSyntheticCredential -APIKey $APIKey
  Deletes all syntheic secure credentials whose key are listed in $Keys from the account associated with the provided API key
.Parameter Key
  The non-secure label used to identify the credential.
.Parameter Value
  The secure value to store in the credential.
.Parameter Description
  User-friendly text used to describe the credential.
#>

Function Remove-NRSyntheticSecureCredential {
  [CMDLetBinding(SupportsShouldProcess=$true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Key
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://synthetics.newrelic.com/synthetics/api/v1/secure-credentials/$Key"

    # Call the API
    If ($PSCmdlet.ShouldProcess($Key, "Remove synthetic secure credential")) {
      Invoke-RestMethod -Uri $url -Headers $headers -Method 'Delete' -ContentType 'application/json'
    }
  }
}

<#
.Synopsis
  Updates a New Relic Synthetic Secure Credential
.Description
  Updates a New Relic synthetic secure credential.
.Example
  Update-NRSyntheticSecureCredential -APIKey $APIKey -Key 'MyKey' -Value 'MyNewSecureValue'
  Updates the credential with key MyKey to the new value specified
.Parameter APIKey
  This must be an Admin API key. See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Key
  The non-secure label used to identify the credential.
.Parameter Value
  The secure value to store in the credential.
.Parameter Description
  User-friendly text used to describe the credential.
#>
Function Update-NRSyntheticSecureCredential {
  [CMDLetBinding(SupportsShouldProcess=$true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Key,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Value,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [string] $Description = $true
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://synthetics.newrelic.com/synthetics/api/v1/secure-credentials/$key"

    # Build up the body of the request
    $Body = @{
      key = $Key.ToUpper()
      value = $Value
      description = $Description
    }

    # Call the API
    If ($PSCmdlet.ShouldProcess($Id, "Update synthetic credential")) {
      Invoke-RestMethod -Uri $url -Headers $headers -Body ($Body | ConvertTo-Json) -Method 'Put' -ContentType 'application/json'
    }
  }
}
