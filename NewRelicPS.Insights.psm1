Function Get-NRInsightsQuery {
    Param (
      [Parameter (Mandatory=$true)]
      [string] $APIKey,
      [Parameter (Mandatory=$true)]
      [string] $AccountID,
      [Parameter (Mandatory=$true)]
      [string] $NRQL
    )

    Begin {
        $URL = "https://insights-api.newrelic.com/v1/accounts/$AccountID/query?nrql="

        # Create header for authorization
        $Headers = @{
            'X-Query-Key' = $APIKey
        }
    }
    Process {
        $encodedQuery = $URL + [System.Web.HTTPUtility]::UrlEncode($NRQL)

        Return Invoke-RestMethod -Uri $encodedQuery -Method Get -Headers $Headers
    }
}