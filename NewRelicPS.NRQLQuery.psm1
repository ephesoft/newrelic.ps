Using module .\NewRelicPS.GraphQLQueries.psm1

<#
.Synopsis
  Returns result of nrql query
.Description
  Returns result of nrql query
.Example
  Invoke-NRQLQuery -AccountId 2789654 -Query 'SELECT * FROM SystemSamples' -APIKey 'fake-api-key'
.Example
  @("SELECT uniques(hostname) AS host_names FROM SystemSample SINCE 1 hour ago limit max","SELECT uniques(agentVersion) from SystemSample") | Invoke-NRQLQuery -AccountId 2789654 -APIKey 'fake-api-key'
.Parameter APIKey
  Can be either the account level REST API key or an admin user's API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter AccountId
  New Relic account ID
.Parameter Query
  NRQL query
#>
Function Invoke-NRQLQuery {
  [CMDLetBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true)]
    [string] $AccountId,
    [Parameter (Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Query
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
    $url = 'https://api.newrelic.com/graphql'
  }
  Process {
    $graphqlQuery = Get-GraphQLQueryNRQLQuery -AccountId $AccountId -Query $Query
    $body = @{
      query = $graphqlQuery
    } | ConvertTo-Json

    $result = Invoke-RestMethod -Uri $url -headers $headers -body $body -Method 'Post' -ContentType 'application/json'
    return $result
  }
}