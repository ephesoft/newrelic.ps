Using module '.\NewRelicPS.Configuration.GetFromYAML.psm1'
Using module '.\NewRelicPS.Configuration.Channel.psm1'
Using module '.\NewRelicPS.Configuration.Dashboard.psm1'
Using module '.\NewRelicPS.Configuration.Policy.psm1'
Using module '.\NewRelicPS.Configuration.NRQLCondition.psm1'
Using module '.\NewRelicPS.Configuration.SyntheticLocationCondition.psm1'
Using module '.\NewRelicPS.Configuration.SyntheticMonitor.psm1'
Using module '.\NewRelicPS.Configuration.SyntheticSecureCredential.psm1'

<#
.Synopsis
  Idempotently applies New Relic configurations to an account
.Description
  Provide the location to the yaml file which declares the configuration and this CMDLet will idempotently apply those New Relic configurations.
  View the module's readme for more information on building the yaml configuration file.
.Example
  Update-NRConfig -AccountID '1234567' -AdminAPIKey $MyAdminKey -PersonalAPIKey $MyPersonalKey -FilePath C:\Path\To\config.yml
  Uses New Relic APIs to idempotently update the New Relic configuration to match what is defined in the file provided.  Any existing supported resources that are not defined will be removed.
  For a list of supported resources and more information on building the yaml configuration file view the module's readme.
.Example
  Update-NRConfig -AccountID '1234567' -AdminAPIKey $MyAdminKey -PersonalAPIKey $MyPersonalKey -FilePath C:\Path\To\config.yml -Parameters $Parameters
  This command passes a hash table of custom parameters to the configuration file.  This is done so that secrets are not stored in plain text.
.Parameter AccountId
  The New Relic account Id where resources will be configured.
.Parameter AdminAPIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter ConfigurationFilePath
  Can be the full path or a relative path to the YAML configuration file.
.Parameter Parameters
  A hash table of parameters that can be passed into the configuration file to be used in place of secrets.  Parameters are wrapped with ${} in the configuration file (i.e. ${MySecret}).
.Parameter PersonalAPIKey
  Must be a personal API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
#>
function Update-NRConfiguration {
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSShouldProcess", '', Justification = "All CMDLets being called have ShouldProcess in place and the preference is passed to them." )]
  [CMDLetBinding(SupportsShouldProcess=$true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AccountId,
    [Parameter (Mandatory = $true)]
    [string] $AdminAPIKey,
    [Parameter (Mandatory = $true)]
    [string] $ConfigurationFilePath,
    [Parameter (Mandatory = $true)]
    [string] $PersonalAPIKey,
    [hashtable] $Parameters = @{},
    [switch] $Force
  )
  Write-Verbose "Getting desired state configuration from $ConfigurationFilePath"
  $config = Get-NRConfigFromYAML -ConfigurationFilePath $ConfigurationFilePath -Parameters $Parameters

  # Exit if the configuration is blank
  If (-Not $Config) {
    Write-Warning "The file at $ConfigurationFilePath does not contain a configuration, no actions performed.  To remove all supported resources, add any item to the yml configuration and specify the Force parameter."
    Return
  }

  Foreach ($key in $config.keys) {
    Write-Verbose "Defined $key count: $($config[$key].count)"
  }

  # Configure the environment
  If ($Force) {
    Write-Warning "Force has been specified, removing all supported resource types without a declaration.  Hope you know what you are doing!"
  }

  # Sync dashboards - Force does not apply to this resource type since it is a sync
  If ($config.Dashboards) {
    Write-Verbose 'Configuring Dashboards'
    Sync-NRDashboardConfiguration -APIKey $PersonalAPIKey -DefinedDashboards $config.Dashboards -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finished Configuring Dashboards'
  }

  # Synthetic Secure Credentials should be processed before Synthetic Monitors
  If ($config.SyntheticSecureCredentials -or $Force) {
    Write-Verbose 'Configuring Synthetic Secure Credentials'
    Set-NRSyntheticSecureCredentialConfiguration -AdminAPIKey $AdminAPIKey -DefinedSyntheticSecureCredentials $config.SyntheticSecureCredentials -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finished Configuring Secure Credentials'
  }

  # Synthetic Monitors must be processed before Synthetic conditions
  If ($config.SyntheticMonitors -or $Force) {
    Write-Verbose 'Configuring Synthetic Monitors'
    Set-NRSyntheticLocationMonitorConfiguration -AdminAPIKey $AdminAPIKey -DefinedSyntheticMonitors $config.SyntheticMonitors -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finished Configuring Synthetic Monitors'
  }

  If ($config.AlertPolicies -or $Force) {
    Write-Verbose 'Configuring Alert Policies'
    Set-NRPolicyConfiguration -APIKey $AdminAPIKey -DefinedPolicies $config.AlertPolicies -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finished Configuring Alert Policies'

    Write-Verbose 'Configuring NRQL Conditions'
    Set-NRQLConditionConfiguration -AdminAPIKey $AdminAPIKey -PersonalAPIKey $PersonalAPIKey -DefinedPolicies $config.AlertPolicies -AccountId $AccountId -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finshed Configuring NRQL Conditions'

    Write-Verbose 'Configuring Synthetic Conditions'
    Set-NRSyntheticLocationConditionConfiguration -AdminAPIKey $AdminAPIKey -DefinedPolicies $config.AlertPolicies -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finished Configuring Synthetic Conditions'
  }

  If ($config.NotificationChannels -or $Force) {
    Write-Verbose 'Configuring Notification Channels'
    Set-NRChannelConfiguration -APIKey $AdminAPIKey -DefinedChannels $config.NotificationChannels -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finished Configuring Notification Channels'
  }

  # Channel policy links need to be processed following policies and channels
  If ($config.AlertPolicies -or $Force) {
    Write-Verbose 'Configuring Channel-Policy links'
    Set-NRChannelPolicyLink -APIKey $AdminAPIKey -DefinedPolicies $config.AlertPolicies -Verbose:$VerbosePreference -WhatIf:$WhatIfPreference
    Write-Verbose 'Finished Configuring Channel-Policy links'
  }

  Write-Output 'Configuration Update Complete.'
}
