# ChangeLog
Version changes are logged here using semantic versioning (https://semver.org/)

### Deprecated features
*These will be removed in the next major release*
 - None

### 1.6.1 (2022-08-04)
- Fixed bug when updating runbookUrl in a condition

### 1.6.0 (2022-08-04)
- Added support for runbookUrl in Conditions

### 1.5.1 (2022-07-11)
- Fixed bug where error is thrown when only one alert policy exists in an account

### 1.5.0 (2022-07-08)
- Added Get-NRAPMEntity, Get-NRNormalizationRule, New-NRNormalizationRule, and Update-NRNormalizationRule

### 1.4.1 (2022-07-05)
- Fixed null reference bug in Synthetic condition when entities are defined but null

### 1.4.0 (2022-06-06)
- Added Get-GraphQLQueryAddTagToEntity and Get-GraphQLQueryCreateMutingRuleBySchedule
- Added New-NRMutingRule
- Added New-NRResourceTag

### 1.3.0 (2022-05-19)
- Added Get-NRApplication and Get-NRHostName
- Added Get-GraphQLQueryApplicationHostName
- Added Invoke-NRQLQuery

### 1.2.1 (2021-01-10)
- Fixed issue with creating NRQL conditions using SUM value function
- Renamed plural functions for linting

### 1.2.0 (2021-07-08)
- Added support for dashboard list pagination

### 1.1.0 (2021-04-01)
- Updated Dashboard cmdlets to support filters

### 1.0.2 (2021-03-22)
 - BREAKING: Updated Dashboard CMDLets to work with New Relic's graphQL API
 - BREAKING: Renamed module for publishing to PSGallery
 - Added Remove-NRDashboard CMDlet
 - Added Restore-NRDashboard CMDlet
 - Added dashboard configuration to Update-NRConfiguration
 - Added unit tests for dashboard CMDlets

### 0.14.0 (2020-12-14)
 - Added support for replacement of variables in synthetic scripts

### 0.13.1 (2020-12-11)
 - Fixed issue with pagination for NRQL conditions

### 0.13.0 (2020-11-02)
-----
 - Changed default alert on signal loss value to false
 - Changed default signal loss expiration duration to 1 hour

### 0.12.1 (2020-10-05)
-----
 - Fixed bug where incorrect number of defined conditions were shown for policy in verbose logging output

### 0.12.0 (2020-09-30)
-----
 - Added CMDLets to manage synthetic monitors with scripts
 - Added CMDLets to idempotently configure Synthetic Monitors using scripts
 - Updated documentation to include the scriptPath parameter for Synthetic monitors

### 0.11.0 (2020-09-26)
-----
 - Added CMDLets for idempotently configuring Synthetic Secure Credentials

### 0.10.0 (2020-09-26)
-----
 - Added CMDLets for managing Synthetic Secure Credentials

### 0.9.1 (2020-09-25)
-----
 - Fixed issue with list of nested modules

### 0.9.0 (2020-09-25)
-----
 - Added CMDLets for managing Synthetic Monitors
 - Added CMDLets for configuration of Synthetic Monitors
 - Added CMDLets for managing Synthetic Location conditions
 - Added CMDLets for configuration of Synthetic Location conditions
 - Updated configuration file documentation to include new features

### 0.8.0 (2020-08-31)
-----
 - Reduce standard output when a notification channel is recreated due to changes


### 0.7.0 (2020-08-25)
-----
 - Added CMDLets for applying a configuration from a yaml file
 - Added documentation on creating a configuration from a yaml file
 - Updated ConditionType parameter to Type for consistency with GraphQL API on New-NRQLCondition and Update-NRQLCondition

### 0.6.0 (2020-08-03)
-----
 - Merged CMDLets for creating and updating conditions so all condition operations utilize the GraphQL API

### 0.5.0 (2020-07-31)
-----
 - Added CMDLets for baseline conditions
 - Merged CMDLets for getting NRQL conditions into Get-NRQLCondition
 - Merged CMDLets for removing any type of condition into Remove-NRCondition
 - Added tests covering CMDLet logic

### 0.4.0 (2020-07-28)
-----
 - Added NRQL static condition CMDLets
 - Added tests covering CMDLet logic

### 0.3.0 (2020-07-23)
-----
 - Added notification channel CMDLets
 - Added policy CMDLets
 - Added unit tests for rest call wrappers with Pester 5.x support
 - Added tests for new CMDLet logic
 - Renamed NewRelic.Dashboard module to NewRelic.Dashboards for consistency
 - Renamed existing 'NewRelic*' commands to 'NR' for consistency
 - Updated to Powershell Publish pipe v3

### 0.2.0
-----
 - Added Get-NewRelicInsightsQuery
 - Added LICENSE.md

### 0.1.0
-----
 - Initial Commit

- - - - -
Check the [Markdown Syntax Guide](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html) for basic syntax.