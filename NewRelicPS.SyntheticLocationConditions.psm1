<#
.Synopsis
  Gets New Relic Synthetic Location Conditions
.Description
  Gets all New Relic synthetic conditions in a policy or returns teh condition matching the provided Id.
.Example
  Get-NRSyntheticLocationcondition -APIKey $APIKey -Policy 12345
  Gets information on all New Relic synthetic location conditions in the policy 12345 on the account associated with the provided API Key
.Example
  Get-NRSyntheticCondition -APIKey $APIKey -Id 123
  Returns only data for the condition with Id 123 in the account associated with the API key provided.  Returns null if no existing condition has the ID provided.
.Parameter APIKey
  This must be an Admin API key. See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Id
  If specified, returns a information on the specified condition.
.Parameter PolicyId
  The Id for the policy where conditions are stored.
#>
Function Get-NRSyntheticLocationCondition {
  [CMDLetBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $PolicyId,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [string] $Id
  )
  Begin {

    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_location_failure_conditions/policies/$PolicyId.json"

    # Call the API and return null if a condition ID is provided but not found
    [array]$results += (Invoke-RestMethod -Uri $url -Headers $headers -Method 'Get' -ContentType 'application/json').location_failure_conditions

    # If an Id is provided, return only that item otherwise return all results
    $Id ? ($results | Where-Object {$_.id -eq $id}) : $results
  }
}

<#
.Synopsis
  Creates a New Relic Synthetic Location Condition
.Description
  Creates a New Relic synthetic location condition.
.Example
  New-NRSyntheticLocationCondition -APIKey $APIKey -Name 'MySyntheticLocationCondition' -PolicyID '12345' -CriticalThreshold 3 -Entities @(12345,12346,12347,12348)
  Creates a new Synthetics location condition which opens a violation if 3 or more entities report a synthetics failure.
.Parameter APIKey
  This must be an Admin API key. See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter CriticalThreshold
  The minimum number of monitor locations that must be concurrently failing before a critical violation is opened.
.Parameter Enabled
  Specifies whether the condition is created as enabled or not.  Defaults to false.
.Parameter Entities
  An array of synthetic monitor GUIDs to alert on.
.Parameter Name
  The friendly name used to identify the condition.
.Parameter PolicyId
  The ID for the policy where conditions are stored.
.Parameter Runbook_URL
  A URL to display in notificaitons, most often used to link to a runbook to be executed when a notification is recieved.
.Parameter ViolationTimeLimitSeconds
  The maximum number of seconds a violation can remain open before being closed by the sytem.
.Parameter WarningThreshold
  The minimum number of monitor locations that must be concurrently failing before a warning violation is opened.
#>
Function New-NRSyntheticLocationCondition {
  [CMDLetBinding(SupportsShouldProcess=$true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Name,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [int32] $PolicyId,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [array] $Entities,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [int32] $CriticalThreshold,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [boolean] $Enabled = $true,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [string] $Runbook_URL,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [int32] $Violation_Time_Limit_Seconds,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [int32] $WarningThreshold
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_location_failure_conditions/policies/$PolicyId.json"

    # Build up the body of the request using required or default parameters
    $Body = @{
      location_failure_condition = @{
        name = $Name
        enabled = $Enabled
        entities = $Entities
        terms = @(@{
          priority = 'critical'
          threshold = $CriticalThreshold
        })

      }
    }


    # Add optional parameters
    Switch ($true) {
      {-NOT [string]::IsNullOrWhiteSpace($Runbook_URL)} {$Body += @{runbook_url = $Runbook_URL}}
      {$WarningThreshold} {$Body.location_failure_condition.terms += @{priority = 'warning'; threshold = $WarningThreshold}}
      {$Violation_Time_Limit_Seconds} {$Body += @{violation_time_limit_seconds = $Violation_Time_Limit_Seconds}}
    }

    # Call the API
    If ($PSCmdlet.ShouldProcess($Name, "Create synthetic location condition")) {
      Return Invoke-RestMethod -Uri $url -Headers $headers -Body ($Body | ConvertTo-Json -Depth 4) -Method 'Post' -ContentType 'application/json'
    }
  }
}

<#
.Synopsis
  Deletes a New Relic Synthetic Location Condition
.Description
  Deletes a New Relic synthetic location condition.
.Example
  Remove-NRSyntheticLocationCondition -APIKey $APIKey -Id '123456'
  Deletes the synthetic location condition with Id 123456 in the account associated with the provided API key
.Example
  $Ids | Remove-NRSyntheticCondition -APIKey $APIKey
  Deletes all syntheic location conditions whose Id are listed in $Ids from the account associated with the provided API key
.Parameter APIKey
  This must be an Admin API key. See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys.
.Parameter Id
  The unique Id of the condition to remove.
#>
Function Remove-NRSyntheticLocationCondition {
  [CMDLetBinding(SupportsShouldProcess=$true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Id
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_location_failure_conditions/$Id.json"

    # Call the API
    If ($PSCmdlet.ShouldProcess($Id, "Remove synthetic condition")) {
      Return (Invoke-RestMethod -Uri $url -Headers $headers -Method 'Delete' -ContentType 'application/json').location_failure_condition
    }
  }
}


<#
.Synopsis
  Updates a New Relic Synthetic Location Condition
.Description
  Updates a New Relic synthetic location condition.
.Example
  Update-NRSyntheticLocationCondition -APIKey $APIKey -Id '123456' -CriticalThreshold 3
  Updates condition with Id 123456 and sets the minimum number of monitor locations that must be concurrently failing before a violation is opened to 3.
.Example
  $Ids | Update-NRSyntheticCondition -APIKey $APIKey -Enabled $true
  Enables all conditions in the $Ids array.
.Parameter APIKey
  This must be an Admin API key. See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter CriticalThreshold
  The minimum number of monitor locations that must be concurrently failing before a critical violation is opened.
.Parameter Enabled
  Specifies whether the condition is created as enabled or not.  Defaults to false.
.Parameter Entities
  An array of synthetic monitor GUIDs to alert on.
.Parameter Id
  The unique ID of the synthetic location condition to update.
.Parameter Name
  The friendly name used to identify the condition.
.Parameter PolicyId
  The ID for the policy where conditions are stored.
.Parameter Runbook_URL
  A URL to display in notificaitons, most often used to link to a runbook to be executed when a notification is recieved.
.Parameter ViolationTimeLimitSeconds
  The maximum number of seconds a violation can remain open before being closed by the sytem.
.Parameter WarningThreshold
  The minimum number of monitor locations that must be concurrently failing before a warning violation is opened.
#>
Function Update-NRSyntheticLocationCondition {
  [CMDLetBinding(SupportsShouldProcess=$true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [int32] $Id,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [int32] $PolicyId,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [string] $Name,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [array] $Entities,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [int32] $CriticalThreshold,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [boolean] $Enabled,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [string] $Runbook_URL,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [int32] $Violation_Time_Limit_Seconds,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [int32] $WarningThreshold
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_location_failure_conditions/$id.json"

    # Get the existing item values
    $conditionParams = Get-NRSyntheticLocationCondition -APIKey $APIKey -PolicyId $PolicyId -Id $Id

    # Check for a null result
    If (-NOT $conditionParams) {
      Write-Error "Condition with Id $Id was not found in policy $PolicyId.  Does the provided API key have access?"
    }

    # Build up the list of top-level fields to update
    Switch ($true) {
      { $CriticalThreshold } {  ($conditionParams.Terms | Where-Object {$_.priority -eq 'critical'}).threshold = $CriticalThreshold }
      { "$Enabled" } { $conditionParams.enabled = $Enabled }
      { $Entities } { $conditionParams.entities = $Entities }
      { -NOT [string]::IsNullOrWhiteSpace($Name) } { $conditionParams.name = $Name }

      # These items may not exist on the object so add them with a force to overwrite if they are there
      { -NOT [string]::IsNullOrWhiteSpace($Runbook_URL) } {$conditionParams | Add-Member -NotePropertyName 'runbook_url' -NotePropertyValue $Runbook_URL -Force -PassThru}
      { $Violation_Time_Limit_Seconds } { $conditionParams | Add-Member -NotePropertyName 'violation_time_limit_seconds' -NotePropertyValue $Violation_Time_Limit_Seconds -Force }
    }

    # The warning term is an optional set of parameters so process that separately
    # The user can specify zero to remove the warning threshold
    If ($WarningThreshold -eq 0) {
      [array]$conditionParams.terms = $conditionParams.terms | Where-Object {$_.priority -ne 'warning'}
    }
    ElseIf ($WarningThreshold) {

      # Update the existing warning term's threshold if it already exists
      If ($conditionParams.Terms | Where-Object {$_.priority -eq 'warning'}) {
        ($conditionParams.terms | Where-Object {$_.priority -eq 'warning'}).threshold = $WarningThreshold
      }

      # Otherwise add the warning term to the item
      Else {
        $conditionParams.terms += @{
          priority = 'warning'
          threshold = $WarningThreshold
        }
      }
    }

    # The API requires parameters to be wrapped in the location_failure_condition parent object
    $body = [pscustomobject]@{
      location_failure_condition = $conditionParams
    } | ConvertTo-Json -Depth 4

    # Call the API
    If ($PSCmdlet.ShouldProcess($Id, "Update synthetic condition")) {
      Return (Invoke-RestMethod -Uri $url -Headers $headers -Body $body -Method 'Put' -ContentType 'application/json').location_failure_condition
    }
  }
}
