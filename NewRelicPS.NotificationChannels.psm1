<#
.Synopsis
  Gets a New Relic notification channel
.Description
  Returns data on all New Relic notification channels or a single channel
.Example
  Get-NRNotificationChannel -APIKey $APIKey
  Returns information for all channels in the account associated with the provided API key
.Example
  Get-NRNotificationChannel -APIKey $APIKey -Name 'MyChannel'
  Returns information for the channel named MyChannel in the account associated with the provided API key
.Parameter APIKey
  Can be either the account level REST API key or an admin user's API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Name
  If provided, filters the returned data to the channel name specified.
#>
Function Get-NRNotificationChannel {
  [CMDLetBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Name
  )
  Begin {
    $url = 'https://api.newrelic.com/v2/alerts_channels.json'

    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $Result = (Invoke-RestMethod $url -headers $headers -method 'Get').channels

    # The API doesn't have a filter option, so filter the list of results to the specified name
    If ($Name) {
      Return $Result | Where-Object {$_.name -eq $Name}
    }
    Else {
      Return $Result
    }
  }
}

<#
.Synopsis
  Creates a New Relic notification channel
.Description
  Creates a New Relic notification channel
.Example
  New-NRNotificationChannel -APIKey $APIKey -Name 'MyChannel' -Type 'Email' -Configuration $JSONConfiguration
  Creates a new emmail notification channel.  For a list of channel types and their JSON configurtions see https://rpm.newrelic.com/api/explore/alerts_channels/create
.Parameter APIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Type
  The type of notification channel to create.  The CMDLet will only let you specify valid types.
.Parameter Configuration
  This is a JSON-formatted configuration.  For a list of channel types and their JSON configurtions see https://rpm.newrelic.com/api/explore/alerts_channels/create
#>
Function New-NRNotificationChannel {
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Name,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [ValidateSet ('User','Email','OpsGenie','PagerDuty','Slack','VicorOps','Webhook','xMatters')]
    [string] $Type,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [hashtable] $Configuration
  )
  Begin {
    $url = 'https://api.newrelic.com/v2/alerts_channels.json'
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $body = @{
      'channel' = [ordered]@{
        'name'          = $Name
        'type'          = $Type
        'configuration' = $Configuration
      }
    } | ConvertTo-Json
    if ($PSCmdlet.ShouldProcess($Name,'Create Notification Channel')) {
      Return (Invoke-RestMethod $url -headers $headers -body $body -method 'Post' -ContentType 'application/json').channels
    }
  }
}

<#
.Synopsis
  Removes a New Relic notification channel
.Description
  Removes a New Relic notification channel
.Example
  Remove-NRNotificationChannel -APIKey $APIKey -ChannelId 1234
  Removes the notification channel with Id 1234 from the account associated with the provided API Key
.Example
  $ChannelsToRemove.Id | Remove-NRNotificationChannel -APIKey $APIKey
  Removes all channels in $ChannelsToRemove from the account assocaited with the provided API key
.Parameter APIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Type
  The type of notification channel to create.  The CMDLet will only let you specify valid types.
.Parameter Configuration
  This is a JSON-formatted configuration.  For a list of channel types and their JSON configurtions see https://rpm.newrelic.com/api/explore/alerts_channels/create
#>
Function Remove-NRNotificationChannel {
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $ChannelId
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_channels/$ChannelId.json"
    if ($PSCmdlet.ShouldProcess($ChannelId,'Delete Notification Channel')) {
      Return Invoke-RestMethod $url -headers $headers -method 'Delete'
    }
  }
}
