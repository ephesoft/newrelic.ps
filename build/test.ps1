  Install-Module PSScriptAnalyzer -Force
  Install-Module Pester -Force
  Install-Module PowerShell-Yaml -Force
  Import-Module PSScriptAnalyzer
  Import-Module Pester
  Import-Module PowerShell-Yaml
  $PesterConfig = [PesterConfiguration]::Default
  $PesterConfig.Output.Verbosity = 'Detailed'
  $PesterConfig.Run.Path = '.\*'
  $PesterConfig.Run.Exit = $true
  Invoke-Pester -Configuration $PesterConfig