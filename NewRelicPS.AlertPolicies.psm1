<#
.Synopsis
  Gets New Relic alert policy information
.Description
  Gets all New Relic alert policy or a single alert policy
.Example
  Get-NRAlertPolicy -APIKey $APIKey
  Gets information on all New Relic alert policies in the account associated with the API key provided
.Example
  Get-NRAlertPolicy -APIKey $APIKey -Name 'MyPolicy'
  Returns only data for the alert policy with name MyPolicy in the account associated with the API key provided
.Parameter APIKey
  Can be either the account level REST API key or an admin user's API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter Name
  If provided, filters returned data to only the alert policy matching the name specified
#>
Function Get-NRAlertPolicy {
  [CMDLetBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Name
  )
  Begin {
    $url = 'https://api.newrelic.com/v2/alerts_policies.json'

    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $Result = (Invoke-RestMethod $url -headers $headers -method 'Get').policies

    # The API doesn't have a filter option, so filter the list of results to the specified name
    If ($Name) {
      Return $Result | Where-Object {$_.name -eq $Name}
    }
    Else {
      Return $Result
    }
  }
}

<#
.Synopsis
  Creates a New Relic alert policy
.Description
  Creates a New Relic alert policy
.Example
  New-NRAlertPolicy -APIKey $APIKey -Name 'MyPolicy' -IncidentPreference 'PER_CONDITION_AND_TARGET'
  Creates a new alert policy named 'MyPolicy' with the incident preference PER_CONDITION_AND_TARGET
.Example
  $MyPolicies | New-NRAlertPolicy -APIKey $APIKey
  Creates all the policies defined in $MyPolicies as long as each has a Name and IncidentPreference attribute
.Parameter APIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter IncidentPreference
  Determines how New Relic Alerts will create incidents and group violations. See more here: https://docs.newrelic.com/docs/alerts/new-relic-alerts-beta/reviewing-alert-incidents/specify-when-new-relic-creates-incidents
#>
Function New-NRAlertPolicy {
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Name,
    [Parameter (ValueFromPipelineByPropertyName = $true)]
    [ValidateSet ('PER_POLICY', 'PER_CONDITION', 'PER_CONDITION_AND_TARGET')]
    [string] $IncidentPreference = 'PER_CONDITION_AND_TARGET'
  )
  Begin {
    $url = 'https://api.newrelic.com/v2/alerts_policies.json'
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $body = @{
      'policy' = [ordered]@{
        'name'                = $Name
        'incident_preference' = $IncidentPreference
      }
    } | ConvertTo-Json
    if ($PSCmdlet.ShouldProcess($Name, 'Create Alert Policy')) {
      Return (Invoke-RestMethod $url -headers $headers -body $body -method 'Post' -ContentType 'application/json').policy
    }
  }
}

<#
.Synopsis
  Removes a New Relic alert policy
.Description
  Removes a New Relic alert policy
.Example
  Remove-NRAlertPolicy -APIKey $APIKey -PolicyId 1234
  Removes the alert policy with id 1234 in the account associated with the API key specified
.Example
  $PoliciesIdsToRemove | Remove-NRAlertPolicy -APIKey $APIKey
  Removes all policies in the array of ids provided from the account associated with the API key specified
.Parameter APIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter PolicyId
  The New Relic assiged Id for the policy to be removed
#>
Function Remove-NRAlertPolicy {
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $PolicyId
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_policies/$PolicyId.json"
    if ($PSCmdlet.ShouldProcess($PolicyId,'Delete Alert Policy')) {
      Return (Invoke-RestMethod $url -headers $headers -method 'Delete').policy
    }
  }
}

<#
.Synopsis
  Updates a New Relic alert policy
.Description
  Creates a New Relic alert policy
.Example
  Update-NRAlertPolicy -APIKey $APIKey -PolicyId 1234 -Name 'MyPolicy' -IncidentPreference 'PER_CONDITION'
  Updates the alert policy with ID 1234 to have the name 'MyPolicy and incident preference PER_CONDITION
.Parameter APIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter IncidentPreference
  Determines how New Relic Alerts will create incidents and group violations. See more here: https://docs.newrelic.com/docs/alerts/new-relic-alerts-beta/reviewing-alert-incidents/specify-when-new-relic-creates-incidents
#>
Function Update-NRAlertPolicy {
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $PolicyId,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $Name,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [ValidateSet ('PER_POLICY', 'PER_CONDITION', 'PER_CONDITION_AND_TARGET')]
    [string] $IncidentPreference
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_policies/$PolicyId.json"
    $body = @{
      'policy' = [ordered]@{
        'name'                = $Name
        'incident_preference' = $IncidentPreference
      }
    } | ConvertTo-Json
    if ($PSCmdlet.ShouldProcess($Name, 'Create Alert Policy')) {
      Return (Invoke-RestMethod $url -headers $headers -body $body -method 'Put' -ContentType 'application/json').policy
    }
  }
}

<#
.Synopsis
  Creates a New Relic Channel/Policy link
.Description
  Creates a New Relic Channel/Policy link
.Example
  Add-NRNotificationChannelToAlertPolicy -APIKey $APIKey -PolicyId 1234 -ChannelId 5678
  Creates a new link between the policy with Id 1234 and the channel with Id 5678 in the account associated with the API key specified
.Example
  $LinksToCreate | Add-NRNotificationChannelToAlertPolicy -APIKey $APIKey
  Creates all of the links specified in $LinksToCreate so long as each item in the array has a PolicyId and ChannelId property
.Parameter APIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter PolicyId
  The New Relic assiged Id for the policy associated to the link to be created
.Parameter ChannelId
  The New Relic assiged Id for the channel associated to the link to be created
#>
Function Add-NRNotificationChannelToAlertPolicy {
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $PolicyId,
    [Parameter (Mandatory = $true)]
    [array] $ChannelIds
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_policy_channels.json?policy_id=$PolicyId&channel_ids=$($ChannelIds -join (','))"
    if ($PSCmdlet.ShouldProcess($Name, 'Create Alert Policy')) {
      Return (Invoke-RestMethod $url -headers $headers -method 'Put' -ContentType 'application/json').policy
    }
  }
}

<#
.Synopsis
  Removes a New Relic Channel/Policy link
.Description
  Removes a New Relic Channel/Policy link
.Example
  Remove-NRNotificationChannelToAlertPolicy -APIKey $APIKey -PolicyId 1234 -ChannelId 5678
  Removes a new link between the policy with Id 1234 and the channel with Id 5678 in the account associated with the API key specified
.Example
  $LinksToRemove | Add-NRNotificationChannelToAlertPolicy -APIKey $APIKey
  Removes all of the links specified in $LinksToCreate so long as each item in the array has a PolicyId and ChannelId property
.Parameter APIKey
  Must be an admin user's API Key, not an account-level REST API Key.  See more here: https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys
.Parameter PolicyId
  The New Relic assiged Id for the policy associated to the link being removed
.Parameter ChannelId
  The New Relic assiged Id for the channel associated to the link being removed
#>
Function Remove-NRNotificationChannelFromAlertPolicy {
  [CMDLetBinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $APIKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $PolicyId,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string] $ChannelId
  )
  Begin {
    $headers = @{
      'X-Api-Key' = $APIKey
    }
  }
  Process {
    $url = "https://api.newrelic.com/v2/alerts_policy_channels.json?policy_id=$PolicyId&channel_id=$ChannelId"
    if ($PSCmdlet.ShouldProcess("Policy $PolicyId and Channel $ChannelId", 'Remove Channel-Policy Link')) {
      Return Invoke-RestMethod $url -headers $headers -method 'Delete' -ContentType 'application/json'
    }
  }
}