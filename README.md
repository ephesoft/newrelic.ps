# NewRelicPS
This powershell module can be used by running Import-Module in a PowerShell console

## Contributing
Please see the contributing.md file

## Usage
### Discovering Available CMDLets
To view the available CMDLets in this module:
`Get-Command -Module NewRelicPS`

All CMDLets have detailed help available with examples.  To view more information on a specific command view the CMDLets full help:
`Get-Help Get-NRQLCondition -Full`

### About API Keys
New Relic APIs use a few different [types of API Keys](https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys) depending on the types of actions being taken.   You'll want to make sure to use the correct API key for the CMDLet being executed.  All CMDlets specify the type of API Key required in parameter help.

To view CMDLet help:
`Get-Help Get-NRQLCondition -Full`
### Working with Conditions
The New Relic APIs are a bit inconsistent in functionality at the time of this writing.  Some APIs can work with all conditions but others can only work with certain types of conditions.  CMDLet names indicate what types of conditions a CMDlet can be used for but you'll want to check CMDLet help to ensure you're using the correct [type of API key](https://docs.newrelic.com/docs/apis/get-started/intro-apis/types-new-relic-api-keys).

For exampe, consider the following CMDLets
* `Remove-NRCondition`: Can be used with any condition and uses a 'Personal' API Key
* `Get-NRQLCondition`: Works specifically with NRQL conditions and uses a 'Personal' API Key
* `New-NRAlertPolicy`: Requires an 'Admin' API key
* `Update-NRNotificationChannel`: Requires an 'Admin' API Key

## Automating New Relic Configuration
You can use this module to build a yaml configuration file to consistently apply and maintain your New Relic configuration.  This is very useful when you have many alert policies, notification channels, and conditions and want to keep the configuration consistent, especially across multiple New Relic accounts.

### Limitations
Currently only these resource types are support:
  * Alert policies
    * Only opsgenie notification channels have been tested, however, other channel types may work without changes.
  * Dashboards
  * Notification channels
  * NRQL conditions
  * Synthetics
  * Secure Credentials

Please feel free to try this out and contribute if you'd like to extend support for other configuration areas.


### How It Works
You define the configuration in a yaml file and then run the `Update-NRConfiguration` CMDLet and provide it the path to the yaml file and your credentials.  The configuration defined in the yml file is then applied to the New Relic account associated with the provided keys.  All resources are created/updated to match the defined configuration and extra resources are removed.

### Applying a Configuration
You can apply a defined configuration with a single command like this:
```PowerShell
Update-NRConfiguration `
  -AccountId '12345' `
  -AdminAPIKey $MyAPIKey `
  -ConfigurationFilePath 'c:\path\to\configuration.yml' `
  -Parameters $MyParameters `
  -PersonalAPIKey $PersonalAPIKey
```
In the example above, sensitive values are being passed into the configuration as [parameters](docs/PARAMETERS.md) via the $MyParameters hash table.

The CMDLet uses both the New Relic Rest API as well as the Nerd Graph API so two different API keys are currently required.

### How To Build a Configuration File
See the [configuration file](docs/CONFIGURATIONFILE.md) for supported resources and syntax.