
<#
.Synopsis
  Retrieves a New Relic configuration from YAML
.Description
  Retrieves a New Relic configuration from YAML
.Example
  Get-NRConfigFromYAML -FilePath 'c:\path\to\myfile.yml'
  Retrieves a desired New Relic configuration state from YAML as an array of configuraiton objects.
.Parameter FilePath
  Can be the full path or a relative path to the YAML configuration file
#>
function Get-NRConfigFromYAML {
  [CMDLetBinding()]
  Param (
    [Parameter (Mandatory=$true)]
    [string]$ConfigurationFilePath,
    [hashtable]$Parameters = @{}
  )
  $file = Get-Content $ConfigurationFilePath
  Foreach ($line in $file) {
    If ($line -match '\${(.*)}') {
      $line = $line.replace($matches[0],($Parameters[$matches[1]]))
    }
    $content += "`n" + $line
  }
  Return ConvertFrom-Yaml $content
}