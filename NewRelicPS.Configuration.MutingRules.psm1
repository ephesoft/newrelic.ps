Using module .\NewRelicPS.GraphQLQueries.psm1

<#
.Synopsis
    Creates a muting rule in new relic
.Description
    Creates a muting rule in new relic based on specific tag based key-value pairs to supress any notification
.Example
    New-NRMutingRule -AccountId '123456' -TagKey 'clientDomainName' -TagValue 'testing.ephesoft.cloud' -APIKey 'fake-api-key' -RuleName 'Test Muting Rule' -RuleDescription 'Test Muting Rule' -StartTime '2020-12-01T12:00:00' -EndTime '2020-12-01T12:30:00' -TimeZone UTC
.Parameter AccountId
    New Relic account id where a muting rule needs to be created
.Parameter TagKey
    Key name from entity tag based on which a muting rule will be created
.Parameter TagValue
    Value for the key
.Parameter APIKey
    New Relic service user api key
.Parameter RuleName
    Name of the muting rule
.Parameter RuleDescription
    Description of the muting rule
.Parameter StartTime
    The datetime stamp that represents when the muting rule starts. This is in local ISO 8601 format without an offset. Example: '2020-07-08T14:30:00'
.Parameter EndTime
    The datetime stamp that represents when the muting rule ends. This is in local ISO 8601 format without an offset. Example: '2020-07-15T14:30:00'
.Parameter TimeZone
    The time zone that applies to the muting rule schedule. Example: 'America/Los_Angeles'
#>
Function New-NRMutingRule {
    [CMDLetBinding(SupportsShouldProcess = $true)]
    [CMDLetBinding()]
    Param (
        [Parameter (Mandatory = $true)]
        [string] $AccountId,
        [Parameter (Mandatory = $true)]
        [string] $TagKey,
        [Parameter (Mandatory = $true)]
        [string] $TagValue,
        [Parameter (Mandatory = $true)]
        [string] $APIKey,
        [Parameter (Mandatory = $true)]
        [string] $RuleName,
        [Parameter (Mandatory = $true)]
        [string] $StartTime,
        [Parameter (Mandatory = $true)]
        [string] $EndTime,
        [Parameter (Mandatory = $true)]
        [string] $TimeZone,
        [string] $RuleDescription
    )
    Begin {
        $headers = @{
            'X-Api-Key' = $APIKey
        }
        $url = 'https://api.newrelic.com/graphql'
    }
    Process {
        $graphqlQuery = Get-GraphQLQueryCreateMutingRuleBySchedule -AccountId $AccountId -TagKey $TagKey -TagValue $TagValue -RuleName $RuleName -TimeZone $TimeZone -StartTime $StartTime -EndTime $EndTime -RuleDescription $RuleDescription
        $body = @{
            query = $graphqlQuery
        } | ConvertTo-Json
        if ($PSCmdlet.ShouldProcess($RuleName, 'Create Muting Rule')) {
            $result = Invoke-RestMethod -Uri $url -headers $headers -body $body -Method 'Post' -ContentType 'application/json'
            return $result
        }
    }
}