BeforeAll {
  Import-Module '.\NewRelicPS.NRQLConditions.psm1' -Force
  Import-Module '.\NewRelicPS.GraphQLQueries.psm1' -Force

  $apiKey = 'Fake-API-Key'
  $accountId = '12345678'
  $conditionId = '4321'
  $policyId = '1234'
  $name = 'MyCondition'
  $nrqlQuery = 'SELECT count(*) FROM AwsLambdaInvocation'
  $criticalThreshold = '100'
  $warningThreshold = '90'
  $valueFunction = 'single_value'
}

Describe 'NRQL Conditions CMDLets Logic' {
  $testCases = @(
    @{
      TestType = 'PSFilter'
      ParameterName = 'Type'
      PropertyName = 'Type'
      ExpectedResult = 'BASELINE'
      Params = [pscustomobject] @{
        Type = 'BASELINE'
      }
    },
    @{
      TestType = 'PSFilter'
      ParameterName = 'Type'
      PropertyName = 'type'
      ExpectedResult = 'STATIC'
      Params = [pscustomobject] @{
        Type = 'STATIC'
      }
    }
    @{
      TestType = 'PSFilter'
      ParameterName = 'Type'
      PropertyName = 'type'
      ExpectedResult = 'OUTLIER'
      Params = [pscustomobject] @{
        Type = 'OUTLIER'
      }
    },
    @{
      TestType = 'PSFilter'
      ParameterName = 'ConditionId'
      PropertyName = 'id'
      ExpectedResult = '1'
      PARAMS = [pscustomobject] @{
        ConditionId = '1'
      }
    },
    @{
      TestType = 'GraphQLFilter'
      ParameterName = 'PolicyId'
      PropertyName = 'policyId'
      ExpectedResult = '5678'
      PARAMS = [pscustomobject] @{
        PolicyId = '5678'
      }
    }
    @{
      TestType = 'GraphQLFilter'
      ParameterName = 'Name'
      PropertyName = 'name'
      ExpectedResult = '"one"'
      PARAMS = [pscustomobject] @{
        Name = 'one'
      }
    }
    @{
      TestType = 'GraphQLFilter'
      ParameterName = 'Query'
      PropertyName = 'query'
      ExpectedResult = '"SELECT count(*) FROM AwsLambdaInvocation"'
      PARAMS = [pscustomobject] @{
        Query = 'SELECT count(*) FROM AwsLambdaInvocation'
      }
    }
  )
  BeforeAll {
    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.NRQLConditions' {
      Return @{
        data = @{
          actor = @{
            account = @{
              alerts = @{
                nrqlConditionsSearch = @{
                  nrqlConditions = @(
                    [pscustomobject]@{
                      name = 'one'
                      type = 'BASELINE'
                      id = '1'
                    },
                    [pscustomobject]@{
                      name = 'two'
                      type = 'STATIC'
                      id = '2'
                    },
                    [pscustomobject]@{
                      name = 'three'
                      type = 'OUTLIER'
                      id = '3'
                    }
                  )
                }
              }
            }
          }
        }
      }
    }
    Mock Get-GraphQLQueryGetNRQLCondition -ModuleName 'NewRelicPS.NRQLConditions' {}
  }
  It 'Get-NRQLCondition: Returns all results filter parameters not given' {
    $result = Get-NRQLCondition -APIKey $apiKey -AccountId $accountId
    $result.count | Should -Be 3
  }

  It 'Get-NRQLCondition: Returns only the requested item when parameter <ParameterName> is set to <ExpectedResult>' -TestCases ($testCases | Where-Object {$_.TestType -eq 'PSFilter'}) {
    $result = $Params | Get-NRQLCondition -APIKey $apiKey -AccountId $accountId
    $result.count | Should -Be 1
    $result.$PropertyName | Should -Be $ExpectedResult
  }

  It 'Get-NRQLCondition: Passes the correct searchCriteria to get the GraphQL query when parameter <ParameterName> is set to <ExpectedResult>' -TestCases ($testCases | Where-Object {$_.TestType -eq 'GraphQLFilter'}) {
    $Params | Get-NRQLCondition -APIKey $apiKey -AccountId $accountId
    Assert-MockCalled Get-GraphQLQueryGetNRQLCondition -ModuleName 'NewRelicPS.NRQLConditions' -ParameterFilter {
      $searchCriteria -like "*$PropertyName`: $ExpectedResult*"
    }
  }

  It 'New-NRQLCondition: Sends warning threshold data when WarningThreshold specified' {
    New-NRQLCondition -AccountId $accountId -APIKey $apiKey -Type 'Static' -PolicyID $policyId -Name $name -Query $nrqlquery -CriticalThreshold $criticalThreshold -WarningThreshold $warningThreshold
    Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.NRQLConditions' -ParameterFilter {
      ($body | ConvertFrom-Json).query -like "*priority: WARNING*"
    }
  }

  It 'New-NRQLCondition: Does not add warning threhold data when no WarningThreshold specified' {
    New-NRQLCondition -AccountId $accountId -APIKey $apiKey -Type 'Static' -PolicyID $policyId -Name $name -Query $nrqlquery -CriticalThreshold $criticalThreshold
    Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.NRQLConditions' -ParameterFilter {
      ($Body | ConvertFrom-Json).query -notlike "*priority: WARNING*"
    }
  }
}

Describe 'Update-NRQLCondition:'{
  $TestCases = @(
    @{
      CaseName = 'WarningThreshold'
      ExpectedUpdateText = '*threshold: 90*'
      Params        = [pscustomobject] @{
        WarningThreshold = '90'
        Type = 'Static'
      }
    },
    @{
      CaseName = 'CriticalThreshold'
      ExpectedUpdateText = '*thresholdDuration: 10*'
      Params        = [pscustomobject] @{
        CriticalDuration = '10'
        Type = 'Outlier'
      }
    },
    @{
      CaseName = 'BaselineDirection, Description, and Enabled'
      ExpectedUpdateText = 'baselineDirection: UPPER_AND_LOWER, description: "Testing", enabled: true'
      Params        = [pscustomobject] @{
        BaseLineDirection = 'UPPER_AND_LOWER'
        Description = 'Testing'
        Enabled = $True
        Type = 'Baseline'
      }
    },
    @{
      CaseName = 'Name and ViolationTimeLimit'
      ExpectedUpdateText = 'name: "MyTestCondition", violationTimeLimit: TWENTY_FOUR_HOURS'
      Params        = [pscustomobject] @{
        Name = 'MyTestCondition'
        ViolationTimeLimit = 'TWENTY_FOUR_HOURS'
        Type = 'Outlier'
      }
    },
    @{
      CaseName = 'CloseViolationsOnExpiration, ExpirationDuration, and OpenViolationOnExpiration'
      ExpectedUpdateText = 'expiration: {closeViolationsOnExpiration: false expirationDuration: 600 openViolationOnExpiration: true }'
      Params = [pscustomobject] @{
        CloseViolationsOnExpiration = $false
        ExpirationDuration = '600'
        OpenViolationOnExpiration = $true
        Type = 'Baseline'
      }
    }
    @{
      CaseName = 'Query and EvaluationOffset'
      ExpectedUpdateText = 'nrql: {evaluationOffset: 6 query: "Select count(*) From somerecord"}'
      Params = [pscustomobject] @{
        Query = 'SELECT count(*) FROM somerecord'
        EvaluationOffset = '6'
        Type = 'Static'
      }
    }
  )

  BeforeAll {
    Mock Get-GraphQLQueryUpdateNRQLCondition -ModuleName 'NewRelicPS.NRQLConditions' {}
    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.NRQLConditions' {}
    Mock Get-NRQLCondition -ModuleName 'NewRelicPS.NRQLConditions' {
      $true
    }
  }
  It 'Adds field data as expected when <CaseName> specified' -TestCases $TestCases{
    $Params | Update-NRQLCondition -APIKey $apiKey -AccountId $accountId -ConditionId $conditionId
    Assert-MockCalled Get-GraphQLQueryUpdateNRQLCondition -ModuleName 'NewRelicPS.NRQLConditions' -ParameterFilter {
      $FieldsToUpdate -like $ExpectedUpdateText
    }
  }
}