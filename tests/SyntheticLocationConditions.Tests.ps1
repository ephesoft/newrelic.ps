BeforeAll {
  Import-Module '.\NewRelicPS.SyntheticLocationConditions.psm1' -Force

  $apiKey = 'Fake-API-Key'
  $conditionId = '4321'
  $policyId = '1234'
  $name = 'MyCondition'
  $entities = @('1234','1235')
  $criticalThreshold = 2
  $warningThreshold = 1
}

Describe 'Synthetic Location Conditions CMDLets Logic' {
  BeforeAll {
    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' {
      Return @{
        location_failure_conditions = @(
          @{
            id = '4321'
            name = 'MyCondition'
            enabled = $true
            entities = @('12345','12346')
            terms = @(
              @{
                priority = 'critical'
                threshold = 2
              }
            )
          },
          @{
            id = '4322'
            name = 'MyCondition2'
            enabled = $true
            entities = @('12345','12346')
            terms = @(
              @{
                priority = 'critical'
                threshold = 2
              },
              @{
                priority = 'warning'
                threshold = 1
              }
            )
          }
        )
      }
    }
  }
  It 'Get-NRSyntheticLocationCondition: Returns all results when filter parameters not given' {
    [array]$result = Get-NRSyntheticLocationCondition -APIKey $apiKey -PolicyId $policyId
    $result.count | Should -Be 2
  }

  It 'Get-NRSyntheticLocationCondition: Returns only the requested item when Id is provided' {
    [array]$result = Get-NRSyntheticLocationCondition -APIKey $apiKey -PolicyId $policyId -Id $conditionId
    $result.count | Should -Be 1
  }

  It 'New-NRSyntheticLocationCondition: Sends warning threshold data when WarningThreshold specified' {
    New-NRSyntheticLocationCondition -APIKey $apiKey -PolicyId $policyId -Name $name -Entities $entities -CriticalThreshold $criticalThreshold -WarningThreshold 15
    Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' -ParameterFilter {
      ($body | ConvertFrom-Json).location_failure_condition.terms.priority -contains 'warning'
    }
  }

  It 'New-NRSyntheticLocationCondition: Does not add warning threhold data when no WarningThreshold specified' {
    New-NRSyntheticLocationCondition -APIKey $apiKey -PolicyId $policyId -Name $name -Entities $entities -CriticalThreshold $criticalThreshold
    Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' -ParameterFilter {
      ($body | ConvertFrom-Json).location_failure_condition.terms.priority -notcontains 'warning'
    }
  }
}

Describe 'Update-NRSyntheticLocationCondition:'{
  $TestCases = @(
    @{
      CaseName = 'Enabled'
      Params        = [pscustomobject] @{
        Enabled = $true
      }
    },
    @{
      CaseName = 'Name'
      Params        = [pscustomobject] @{
        Name = 'MyUpdatedCondition'
      }
    },
    @{
      CaseName = 'Entities'
      Params =  [pscustomobject] @{
        Entities = @('5678')
      }
    }
    @{
      CaseName = 'Name, Entities, Enabled'
      Params = [pscustomobject] @{
        Name = 'MyUpdatedCondition'
        Entities = @('5678')
        Enabled = $true
      }
    }
  )
  $TermTestCases = @(
    @{
      CaseName = 'Critical'
      Priority = 'critical'
      Params = [pscustomobject] @{
        CriticalThreshold = 5
      }
    },
    @{
      CaseName = 'Warning'
      Priority = 'warning'
      Params = [pscustomobject] @{
        WarningThreshold = 1
      }
    }
  )
  BeforeAll {
    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' {}
    Mock Get-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.SyntheticLocationConditions' {
      @{
        id = '4321'
        name = 'MyCondition'
        enabled = $true
        entities = @('12345','12346')
        terms = @(
          @{
            priority = 'critical'
            threshold = 2
          }
        )
      }
    }
  }
  It 'Updates parameters as expected when <CaseName> specified' -TestCases $TestCases{
    $Params | Update-NRSyntheticLocationCondition -APIKey $apiKey -Id $conditionId -PolicyId $policyId
    Foreach ($key in $Params.psobject.properties.name) {
      Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' -ParameterFilter {
        ($body | ConvertFrom-Json).location_failure_condition.$key -eq $Params.$Key
      }
    }
  }

  It 'Updates the <CaseName> term as expected' -TestCases $TermTestCases{
    $Params | Update-NRSyntheticLocationCondition -APIKey $apiKey -Id $conditionId -PolicyId $policyId
    Foreach ($key in $Params.psobject.properties.name) {
      Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' -ParameterFilter {
        (($body | ConvertFrom-Json).location_failure_condition.terms | Where-Object {$_.priority -eq $Priority}).threshold -eq $Params.$Key
      }
    }
  }

  It 'Updates the warning term when a warning term already exists' {
    Mock Get-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.SyntheticLocationConditions' {
      @{
        id = '4321'
        name = 'MyCondition'
        enabled = $true
        entities = @('12345','12346')
        terms = @(
          @{
            priority = 'critical'
            threshold = 2
          },
          @{
            priority = 'warning'
            threshold = 0
          }
        )
      }
    }
    Update-NRSyntheticLocationCondition -APIKey $apiKey -Id $conditionId -PolicyId $policyId -WarningThreshold $warningThreshold
    Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' -ParameterFilter {
      (($body | ConvertFrom-Json).location_failure_condition.terms | Where-Object {$_.priority -eq 'warning'}).threshold -eq $warningThreshold
    }
  }

  It 'Removes the warning term when WarningThreshold is specified as 0' {
    Update-NRSyntheticLocationCondition -APIKey $apiKey -Id $conditionId -PolicyId $policyId -WarningThreshold 0
    Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.SyntheticLocationConditions' -ParameterFilter {
      $null -eq (($body | ConvertFrom-Json).location_failure_condition.terms | Where-Object {$_.priority -eq 'warning'})
    }
  }
}
