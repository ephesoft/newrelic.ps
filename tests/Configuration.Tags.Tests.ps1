BeforeAll {
    Import-Module .\NewRelicPS.Configuration.Tags.psm1
}

Describe "New-NRResourceTag" {
    Context "When tags are created" {
        BeforeAll {
            $data = [PSCustomObject]@{
                data = [PSCustomObject]@{
                    taggingAddTagsToEntity = [PSCustomObject]@{
                        errors = @()
                    }
                }
            }

            Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Configuration.Tags {
                return $data
            }
            Mock -CommandName Get-GraphQLQueryAddTagToEntity -ModuleName NewRelicPS.Configuration.Tags {}
        }

        It "should not return any errors" {
            $errors = New-NRResourceTag -EntityGUID 'Mjg2Njg3OHxJTkZSQXxOQXwFsdFoeN' -TagKey 'test' -TagValue 'test' -APIKey 'fake-api-key'
            $errors.data.taggingAddTagsToEntity.errors | Should -Be $null
            Should -Invoke Get-GraphQLQueryAddTagToEntity -ModuleName NewRelicPS.Configuration.Tags -Exactly 1 -ParameterFilter { $EntityGUID -eq 'Mjg2Njg3OHxJTkZSQXxOQXwFsdFoeN' -and $TagKey -eq 'test' -and $TagValue -eq 'test' }
            Should -Invoke Invoke-RestMethod -ModuleName NewRelicPS.Configuration.Tags -Exactly 1
        }
    }
}