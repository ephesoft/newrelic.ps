BeforeAll {
    Import-Module .\NewRelicPS.Configuration.MutingRules.psm1
}

Describe "New-NRMutingRule" {
    Context "When muting rule is created" {
        BeforeAll {
            $data = [PSCustomObject]@{
                data = [PSCustomObject]@{
                    alertsMutingRuleCreate = [PSCustomObject]@{
                        id = '123456789'
                    }
                }
            }

            Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Configuration.MutingRules {
                return $data
            }

            Mock -CommandName Get-GraphQLQueryCreateMutingRuleBySchedule -ModuleName 'NewRelicPS.Configuration.MutingRules' {}
        }

        It "should return muting rule id" {

            $id = New-NRMutingRule -AccountId '2866878' -TagKey 'domainName' -TagValue 'testing.sandbox.cloud' -APIKey 'fake-api-key' -RuleName 'Test Muting Rule' -RuleDescription 'Test Muting Rule' -StartTime '2022-06-06T13:00:00' -EndTime '2022-06-06T13:15:00' -TimeZone 'UTC'
            $id.data.alertsMutingRuleCreate.id | Should -Be '123456789'
            Should -Invoke Get-GraphQLQueryCreateMutingRuleBySchedule -ModuleName NewRelicPS.Configuration.MutingRules -Exactly 1 -ParameterFilter {
                $AccountId -eq '2866878' -and $TagKey -eq 'domainName' -and $TagValue -eq 'testing.sandbox.cloud' -and $RuleName -eq 'Test Muting Rule' -and $TimeZone -eq 'UTC' -and $StartTime -eq '2022-06-06T13:00:00' -and $EndTime -eq '2022-06-06T13:15:00' -and $RuleDescription -eq 'Test Muting Rule'
            }
            Should -Invoke Invoke-RestMethod -ModuleName NewRelicPS.Configuration.MutingRules -Exactly 1
        }
    }
}