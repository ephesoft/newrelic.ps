Describe 'Dashboards.Common' {
  BeforeAll {
    Import-Module '.\NewRelicPS.Dashboards.Common.psm1' -Force
    $ResourcePath = "$PSScriptRoot\resources\dashboards\"
  }

  Describe 'Update-NRDashboardWidgetAccountID' {
    It 'Updates the Account ID to match the destination' {
      # Update Config
      $sourceConfig = Get-Content "$ResourcePath\SourceDashboard.json" | ConvertFrom-Json -Depth 10
      $response = Update-NRDashboardWidgetAccountID -AccountId 123 -DashboardConfig $sourceConfig

      # Validate Response
      $expectedConfig = Get-Content "$PSScriptRoot\resources\dashboards\UpdatedAccountIDDashboard.json" | ConvertFrom-Json -Depth 10
      ($response | ConvertTo-Json -Depth 10) | Should -Be ($expectedConfig | ConvertTo-Json -Depth 10)
    }
  }

  Describe 'Update-NRDashboardLinkedEntityID' {
    It 'Updates the Entity IDs to match the destination' {
      # Update Config
      $sourceConfig = Get-Content "$ResourcePath\SourceDashboard.json" | ConvertFrom-Json -Depth 10
      $destinationConfig = Get-Content "$PSScriptRoot\resources\dashboards\DestinationDashboard.json" | ConvertFrom-Json -Depth 10
      $response = Update-NRDashboardLinkedEntityID -SourceConfig $sourceConfig -DestinationConfig $destinationConfig

      # Validate Response
      $updatedConfig = Get-Content "$PSScriptRoot\resources\dashboards\UpdatedDashboard.json" | ConvertFrom-Json -Depth 10
      ($response | ConvertTo-Json -Depth 10) | Should -Be ($updatedConfig | ConvertTo-Json -Depth 10)
    }
  }

  Describe 'ConvertTo-NRDashboardCreatePayload' {
    It 'Creates a valid payload for the Create call' {
      # Update Config
      $sourceConfig = Get-Content "$PSScriptRoot\resources\dashboards\SourceDashboard.json" | ConvertFrom-Json -Depth 10
      $response = ConvertTo-NRDashboardCreatePayload -Config $sourceConfig

      # Validate Response
      $CreatePayload = Get-Content "$PSScriptRoot\resources\dashboards\CreatePayload.json" | ConvertFrom-Json -Depth 10
      ($response | ConvertTo-Json -Depth 10) | Should -Be ($CreatePayload | ConvertTo-Json -Depth 10)
    }
  }

  Describe 'ConvertTo-NRDashboardUpdatePayload' {
    It 'Creates a valid payload for the Update call' {
      # Update Config
      $sourceConfig = Get-Content "$PSScriptRoot\resources\dashboards\UpdatedDashboard.json" | ConvertFrom-Json -Depth 10
      $response = ConvertTo-NRDashboardUpdatePayload -Config $sourceConfig

      # Validate Response
      $CreatePayload = Get-Content "$PSScriptRoot\resources\dashboards\UpdatePayload.json" | ConvertFrom-Json -Depth 10
      ($response | ConvertTo-Json -Depth 10) | Should -Be ($CreatePayload | ConvertTo-Json -Depth 10)
    }
  }
}