BeforeAll {
  Import-Module '.\NewRelicPS.NotificationChannels.psm1' -Force
}

Describe 'Notification Channel CMDLets' {
  BeforeAll {
    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.NotificationChannels' {
      Return @{
        channels = @(
          @{
            name = 'one'
          },
          @{
            name = 'two'
          }
        )
      }
    }
  }

  It 'Get-NRNotificationChannel: Returns all results when name parameter not given' {
    $Result = Get-NRNotificationChannel -APIKey 'Fake-API-Key'
    $Result.count | Should -Be 2
  }

  It 'Get-NRNotificationChannel: Returns only the requested item when name parameter given' {
    $Result = Get-NRNotificationChannel -APIKey 'Fake-API-Key' -Name 'one'
    $Result.count | Should -Be 1
    $Result.name | Should -Be 'one'
  }
}
