Describe 'Set-NRChannelConfiguration' {
  BeforeAll {
    Import-Module '.\NewRelicPS.NotificationChannels.psm1' -Force
    Import-Module '.\NewRelicPS.Configuration.Channel.psm1' -Force
    $apiKey = 'Fake-Test-Key'
  }

  Context 'No channels exist' {
    BeforeAll {
      $channel = @{
        Name = 'Test'
        Type = 'OpsGenie'
        ID = '1234'
        Configuration = @{
          api_key = 'FAKE-API-Key'
          tags = @('one','two')
        }
      }
      Mock Get-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' {}
      Mock New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' {}
    }

    It 'Creates the channel' {
      Set-NRChannelConfiguration -APIKey $apiKey -DefinedChannels @($channel) -WarningAction 'SilentlyContinue'
      Assert-MockCalled New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -ParameterFilter {
        $Name -eq $channel.Name -and $Type -eq $channel.Type -and $Configuration -eq $channel.Configuration
      }
    }
  }

  Context 'Some channels exist' {
    BeforeAll {
      Mock New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' {}
      Mock Remove-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' {}
      Mock Get-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' {
        Return @(
          [pscustomobject]@{
            id = '1234'
            name = 'TestChannel1'
            type = 'user'
            configuration = @{
              user_id = '12345678'
            }
            links = @{
              policy_ids = $null
            }
          },
          [pscustomobject]@{
            id = '1235'
            name = 'TestChannel2'
            type = 'opsgenie'
            configuration = @{
              tags = @('Sandbox', 'HighUrgency')
            }
            links = @{
              policy_ids = '1234567'
            }
          },
          [pscustomobject]@{
            id = '1236'
            name = 'TestChannel3'
            type = 'opsgenie'
            configuration = @{
              tags = @('Sandbox', 'MediumUrgency')
            }
            links = @{
              policy_ids = @('1234568','1234569')
            }
          }
        )
      }
      #Mock New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration' {}

    }

    It 'Creates a new channel when one does not already exist' {
      $channel = @{
        Name = 'Test'
        Type = 'opsgenie'
        ID = '1234'
        Configuration = @{
          api_key = 'FAKE-API-Key'
          tags = @('one','two')
        }
      }

      # Mock Compare-Object so that the CMDLet does not throw due to the channel not actually being created
      Mock Compare-Object -ModuleName 'NewRelicPS.Configuration.Channel' {}

      Set-NRChannelConfiguration -APIKey $apiKey -DefinedChannels @($channel) -WarningAction 'SilentlyContinue'
      Assert-MockCalled New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -ParameterFilter {
        $Name -eq $channel.Name -and $Type -eq $channel.Type -and $Configuration -eq $channel.Configuration
      }
    }

    It 'Does nothing when all channels exists and all property values match' {
      $channels = @(
        @{
          Name = 'TestChannel2'
          Type = 'opsgenie'
          ID = '1235'
          Configuration = @{
            api_key = 'FAKE-API-Key'
            tags = @('Sandbox', 'HighUrgency')
          }
        },
        @{
          Name = 'TestChannel3'
          Type = 'opsgenie'
          ID = '1236'
          Configuration = @{
            api_key = 'FAKE-API-Key'
            tags = @('Sandbox', 'MediumUrgency')
          }
        }
      )

      Set-NRChannelConfiguration -APIKey $apiKey -DefinedChannels $channels -WarningAction 'SilentlyContinue'
      Assert-MockCalled New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 0
      Assert-MockCalled Remove-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 0
    }

    It 'Updates the channel when the channel exists but the tags do not match' {
      $channels = @(
        @{
          Name = 'TestChannel2'
          Type = 'opsgenie'
          ID = '1235'
          Configuration = @{
            api_key = 'FAKE-API-Key'
            tags = @('Sandbox', 'HighUrgency', 'MediumUrgency' )
          }
        },
        @{
          Name = 'TestChannel3'
          Type = 'opsgenie'
          ID = '1236'
          Configuration = @{
            api_key = 'FAKE-API-Key'
            tags = @('Sandbox', 'MediumUrgency')
          }
        }
      )
      Set-NRChannelConfiguration -APIKey $apiKey -DefinedChannels $channels -WarningAction 'SilentlyContinue'

      Assert-MockCalled New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 1 -ParameterFilter {
        $Name -eq $channels[0].Name
      }
      Assert-MockCalled Remove-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 0 -ParameterFilter {
        $ChanelId -eq $channels[0].Id
      }
    }

    It 'Updates the channel when the channel exists but the types do not match' {
      $channels = @(
        @{
          Name = 'TestChannel2'
          Type = 'pagerduty'
          ID = '1235'
          Configuration = @{
            api_key = 'FAKE-API-Key'
            tags = @('Sandbox', 'HighUrgency')
          }
        },
        @{
          Name = 'TestChannel3'
          Type = 'opsgenie'
          ID = '1236'
          Configuration = @{
            api_key = 'FAKE-API-Key'
            tags = @('Sandbox', 'MediumUrgency')
          }
        }
      )
      Set-NRChannelConfiguration -APIKey $apiKey -DefinedChannels $channels

      Assert-MockCalled New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 1 -ParameterFilter {
        $Name -eq $channels[0].Name
      }
      Assert-MockCalled Remove-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 1 -ParameterFilter {
        $ChannelId -eq $channels[0].Id
      }
    }

    It 'Removes existing channels when they are not defined in the configuration' {
      $channels = @(
        @{
          Name = 'TestChannel2'
          Type = 'opsgenie'
          ID = '1235'
          Configuration = @{
            api_key = 'FAKE-API-Key'
            tags = @('Sandbox', 'HighUrgency')
          }
        }
      )
      Set-NRChannelConfiguration -APIKey $apiKey -DefinedChannels $channels -WarningAction 'SilentlyContinue'
      Assert-MockCalled New-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 0
      Assert-MockCalled Remove-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 1 -ParameterFilter {
        $ChannelId -eq '1236'
      }
    }
  }
}
