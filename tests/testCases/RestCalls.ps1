# The test cases in this file are loaded into the common Rest Call tests in RestCall.Tests.ps1

# Set static variables
$apiKey = 'FAKE-API-KEY'
$name = 'MyItem'
$incidentPreference = 'PER_CONDITION_AND_TARGET'
$accountId = 12345678
$policyId = 1234
$channelId = 5678
$conditionId = 4321
$dashboardConfig = [pscustomObject] @{
  fakeConfig = 'fake'
}
$monitorId = 98765
$monitorLocations = 'AWS_US_EAST_1','AWS_US_EAST_2'
$channeltype = 'OpsGenie'
$channelconfig = @{TestConfig = $true}
$dashboardId = 1
$body = 'MyBody'
$nrqlQuery = 'SELECT count(*) FROM AwsLambdaInvocation'
$criticalThreshold = '100'
$url = 'https://some.net'
$scriptContent = 'ABC123'

# Configure test cases.  Each test case is run against base tests as well as tests specific for the REST method used.
$TestCases = @(
  @{
    CMDLet = 'Add-NRNotificationChannelToAlertPolicy'
    ModuleName = 'NewRelicPS.AlertPolicies'
    Method = 'Put'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_policy_channels\.json\?policy_id=$policyId&channel_ids=$channelId"
    Params = @{
      APIKey = $apiKey
      PolicyId = $policyId
      ChannelIds = $channelId
    }
  },
  @{
    CMDLet = 'Get-NRDashboard'
    ModuleName = 'NewRelicPS.Dashboards'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      DashboardId = $dashboardId
    }
  },
  @{
    CMDLet = 'Get-NRDashboardList'
    ModuleName = 'NewRelicPS.Dashboards'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
    }
  },
  @{
    CMDLet = 'Get-NRAlertPolicy'
    ModuleName = 'NewRelicPS.AlertPolicies'
    Method = 'Get'
    ExpectedURL = '^https:\/\/api\.newrelic\.com\/v2\/alerts_policies\.json'
    Params = @{
      APIKey = $apiKey
    }
  },
  @{
    CMDLet = 'Get-NRNotificationChannel'
    ModuleName = 'NewRelicPS.NotificationChannels'
    Method = 'Get'
    ExpectedURL = '^https:\/\/api\.newrelic\.com\/v2\/alerts_channels\.json'
    Params = @{
      APIKey = $apiKey
    }
  },
  @{
    CMDLet = 'Get-NRQLCondition'
    ModuleName = 'NewRelicPS.NRQLConditions'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      AccountId = $accountId
    }
  },
  @{
    CMDLet = 'Get-NRSyntheticLocationCondition'
    ModuleName = 'NewRelicPS.SyntheticLocationConditions'
    Method = 'Get'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_location_failure_conditions\/policies\/$policyId.json"
    Params = @{
      APIKey = $apiKey
      PolicyId = $policyId
    }
  },
  @{
    CMDLet = 'Get-NRSyntheticLocationList'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Get'
    ExpectedURL = '^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v1\/locations'
    Params = @{
      APIKey = $apiKey
    }
  },
  @{
    CMDLet = 'Get-NRSyntheticMonitor'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Get'
    ExpectedURL = '^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v3\/monitors'
    Params = @{
      APIKey = $apiKey
    }
  },
  @{
    CMDLet = 'Get-NRSyntheticMonitor'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Get'
    ExpectedURL = "^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v3\/monitors\/$monitorId"
    Params = @{
      APIKey = $apiKey
      Id = $monitorId
    }
  },
  @{
    CMDLet = 'Get-NRSyntheticMonitorScript'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Get'
    ExpectedURL = "^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v3\/monitors\/$monitorId/script"
    Params = @{
      APIKey = $apiKey
      Id = $monitorId
    }
  },
  @{
    CMDLet = 'Get-NRSyntheticSecureCredential'
    ModuleName = 'NewRelicPS.SyntheticSecureCredentials'
    Method = 'Get'
    ExpectedURL = '^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v1\/secure-credentials'
    Params = @{
      APIKey = $apiKey
    }
  },
  @{
    CMDLet = 'New-NRAlertPolicy'
    ModuleName = 'NewRelicPS.AlertPolicies'
    Method = 'Post'
    ExpectedURL = '^https:\/\/api\.newrelic\.com\/v2\/alerts_policies\.json'
    Params = @{
      APIKey = $apiKey
      Name = $name
      IncidentPreference = $incidentPreference
    }
    BodyTests = @{
      policy = [ordered]@{
        name = $name
        incident_preference = $incidentPreference
      }
    }
  },
  @{
    CMDLet = 'New-NRDashboard'
    ModuleName = 'NewRelicPS.Dashboards'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      AccountId = $accountId
      DashboardConfig = $dashboardConfig
    }
  },
  @{
    CMDLet = 'New-NRNotificationChannel'
    ModuleName = 'NewRelicPS.NotificationChannels'
    Method = 'Post'
    ExpectedURL = '^https:\/\/api\.newrelic\.com\/v2\/alerts_channels\.json'
    Params = @{
      APIKey = $apiKey
      Name = $name
      Type = $channeltype
      Configuration = $channelconfig
    }
  },
  @{
    CMDLet = 'New-NRQLCondition'
    ModuleName = 'NewRelicPS.NRQLConditions'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      AccountId = $accountId
      PolicyId = $policyId
      Name = $name
      Query = $nrqlQuery
      CriticalThreshold = $criticalThreshold
      Type = 'Static'
    }
  },
  @{
    CMDLet = 'New-NRSyntheticLocationCondition'
    ModuleName = 'NewRelicPS.SyntheticLocationConditions'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_location_failure_conditions\/policies\/$policyId.json"
    Params = @{
      APIKey = $apiKey
      PolicyId = $policyId
      Name = $name
      Entities = @('1234','1235')
      CriticalThreshold = $criticalThreshold
    }
  },
  @{
    CMDLet = 'New-NRSyntheticMonitor'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Post'
    ExpectedURL = '^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v3\/monitors'
    Params = @{
      APIKey = $apiKey
      Name = $name
      Locations = $monitorLocations
      Uri = $url
    }
  },
  @{
    CMDLet = 'New-NRSyntheticSecureCredential'
    ModuleName = 'NewRelicPS.SyntheticSecureCredentials'
    Method = 'Post'
    ExpectedURL = '^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v1\/secure-credentials'
    Params = @{
      APIKey = $apiKey
      Key = $name
      Value = $apiKey
    }
  },
  @{
    CMDLet = 'Remove-NRAlertPolicy'
    ModuleName = 'NewRelicPS.AlertPolicies'
    Method = 'Delete'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_policies/$policyId\.json"
    Params = @{
      APIKey = $apiKey
      PolicyId = $policyId
    }
  },
  @{
    CMDLet = 'Remove-NRDashboard'
    ModuleName = 'NewRelicPS.Dashboards'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      DashboardId = $dashboardId
    }
  },
  @{
    CMDLet = 'Remove-NRNotificationChannel'
    ModuleName = 'NewRelicPS.NotificationChannels'
    Method = 'Delete'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_channels/$channelId\.json"
    Params = @{
      APIKey = $apiKey
      ChannelId = $channelId
    }
  },
  @{
    CMDLet = 'Remove-NRNotificationChannelFromAlertPolicy'
    ModuleName = 'NewRelicPS.AlertPolicies'
    Method = 'Delete'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_policy_channels\.json\?policy_id=$policyId&channel_id=$channelId"
    Params = @{
      APIKey = $apiKey
      PolicyId = $policyId
      ChannelId = $channelId
    }
  },
  @{
    CMDLet = 'Remove-NRCondition'
    ModuleName = 'NewRelicPS.NRQLConditions'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      AccountId = $accountId
      ConditionId = $conditionId
    }
  },
  @{
    CMDLet = 'Remove-NRSyntheticLocationCondition'
    ModuleName = 'NewRelicPS.SyntheticLocationConditions'
    Method = 'Delete'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_location_failure_conditions\/$conditionId.json"
    Params = @{
      APIKey = $apiKey
      Id = $conditionId
    }
  }
  @{
    CMDLet = 'Remove-NRSyntheticMonitor'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Delete'
    ExpectedURL = '^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v3\/monitors'
    Params = @{
      APIKey = $apiKey
      Id = $monitorId
    }
  },
  @{
    CMDLet = 'Remove-NRSyntheticSecureCredential'
    ModuleName = 'NewRelicPS.SyntheticSecureCredentials'
    Method = 'Delete'
    ExpectedURL = '^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v1\/secure-credentials'
    Params = @{
      APIKey = $apiKey
      Key = $name
    }
  },
  @{
    CMDLet = 'Restore-NRDashboard'
    ModuleName = 'NewRelicPS.Dashboards'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      DashboardId = $dashboardId
    }
  },
  @{
    CMDLet = 'Update-NRAlertPolicy'
    ModuleName = 'NewRelicPS.AlertPolicies'
    Method = 'Put'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/v2\/alerts_policies/$PolicyId\.json"
    Params = @{
      APIKey = $apiKey
      PolicyId = $policyId
      Name = $name
      IncidentPreference = $incidentPreference
    }
  },
  @{
    CMDLet = 'Update-NRDashboard'
    ModuleName = 'NewRelicPS.Dashboards'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      DashboardId = $dashboardId
      DashboardConfig = $dashboardConfig
    }
  },
  @{
    CMDLet = 'Update-NRQLCondition'
    ModuleName = 'NewRelicPS.NRQLConditions'
    Method = 'Post'
    ExpectedURL = "^https:\/\/api\.newrelic\.com\/graphql"
    Params = @{
      APIKey = $apiKey
      AccountId = $accountId
      ConditionId = $conditionId
      Type = 'Static'
      Name = $name
    }
  },
  @{
    CMDLet = 'Update-NRSyntheticMonitor'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Patch'
    ExpectedURL = "^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v3\/monitors\/$monitorId"
    Params = @{
      APIKey = $apiKey
      Id = $monitorId
      Name = $name
    }
    BodyTests = @{
      name = $name
    }
  },
  @{
    CMDLet = 'Update-NRSyntheticMonitorScript'
    ModuleName = 'NewRelicPS.SyntheticMonitors'
    Method = 'Put'
    ExpectedURL = "^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v3\/monitors\/$monitorId/script"
    Params = @{
      APIKey = $apiKey
      Id = $monitorId
      ScriptContent = $scriptContent
    }
  },
  @{
    CMDLet = 'Update-NRSyntheticSecureCredential'
    ModuleName = 'NewRelicPS.SyntheticSecureCredentials'
    Method = 'Put'
    ExpectedURL = "^https:\/\/synthetics\.newrelic\.com\/synthetics\/api\/v1\/secure-credentials/$name"
    Params = @{
      APIKey = $apiKey
      Key = $name
      Value = $apiKey
    }
  }
)

return $TestCases