Function Get-SyntheticLocationConditionTestCases {
  Return @(
    @{
      propertyName = 'CriticalThreshold'
      expectedValue = 15
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'SyntheticLocation'
              name = 'TestCondition1'
              enabled = $true
              entities = @('Monitor1', 'Monitor2')
              terms = @(
                @{
                  priority = 'critical'
                  threshold = 15
                }
              )
            }
          )
        }
      )
    },
    @{
      propertyName = 'WarningThreshold'
      expectedValue = 1
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'SyntheticLocation'
              name = 'TestCondition1'
              enabled = $true
              entities = @('Monitor1', 'Monitor2')
              terms = @(
                @{
                  priority = 'critical'
                  threshold = 5
                },
                @{
                  priority = 'warning'
                  threshold = 1
                }
              )
            }
          )
        }
      )
    },
    @{
      propertyName = 'Runbook_URL'
      expectedValue = 'https://some.net'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'SyntheticLocation'
              name = 'TestCondition1'
              enabled = $true
              entities = @('Monitor1', 'Monitor2')
              terms = @(
                @{
                  priority = 'critical'
                  threshold = 5
                }
              )
              runbook_url = 'https://some.net'
            }
          )
        }
      )
    },
    @{
      propertyName = 'Enabled'
      expectedValue = $false
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'SyntheticLocation'
              name = 'TestCondition1'
              enabled = $false
              entities = @('Monitor1', 'Monitor2')
              terms = @(
                @{
                  priority = 'critical'
                  threshold = 5
                }
              )
            }
          )
        }
      )
    },
    @{
      propertyName = 'Entities'
      expectedValue = '1234'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'SyntheticLocation'
              name = 'TestCondition1'
              enabled = $true
              entities = @('Monitor1')
              terms = @(
                @{
                  priority = 'critical'
                  threshold = 5
                }
              )
            }
          )
        }
      )
    },
    @{
      propertyName = 'Violation_Time_Limit_Seconds'
      expectedValue = '86400'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'SyntheticLocation'
              name = 'TestCondition1'
              enabled = $true
              entities = @('Monitor1', 'Monitor2')
              terms = @(
                @{
                  priority = 'critical'
                  threshold = 5
                }
              )
              Violation_Time_Limit_Seconds = 86400
            }
          )
        }
      )
    }
  )
}