Function Get-NRQLConditionTestCases {
  Return @(
    @{
      propertyName = 'Criticalduration'
      expectedValue = 120
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'criticalThreshold'
      expectedValue = 180
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 180
                thresholdDuration = 60
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'warningThreshold'
      expectedValue = 60
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'WARNING'
                threshold = 60
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'type'
      expectedValue = 'baseline'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'baseline'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'query'
      expectedValue = 'SELECT average(count(*)) FROM AwsLambdaInvocation'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT average(count(*)) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'criticalOperator'
      expectedValue = 'BELOW'
      definedPolicies = @(
        @{
          Name = 'Test1'

          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
                operator = 'BELOW'
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'description'
      expectedValue = 'test condition'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              description = 'test condition'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'ViolationTimeLimit'
      expectedValue = 'ONE_HOUR'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
              violationTimeLimit = 'ONE_HOUR'
            }
          )
        }
      )
    },
    @{
      propertyName = 'criticalOccurrences'
      expectedValue = 'AT_LEAST_ONCE'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
                thresholdOccurrences = 'AT_LEAST_ONCE'
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'expirationDuration'
      expectedValue = 580
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              expiration = @{
                expirationduration=580
              }
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'closeViolationsOnExpiration'
      expectedValue = $false
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              expiration = @{
                closeViolationsOnExpiration = $false
              }
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'openViolationOnExpiration'
      expectedValue = $false
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              expiration = @{
                openViolationOnExpiration = $false
              }
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'evaluationOffset'
      expectedValue = 5
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
                evaluationOffset = 5
              }
            }
          )
        }
      )
    },
    @{
      propertyName = 'runbookURL'
      expectedValue = 'https://some.runbook.com/fakeurl'
      definedPolicies = @(
        @{
          Name = 'Test1'
          Conditions = @(
            @{
              type = 'static'
              name = 'TestCondition1'
              runbookUrl = 'https://some.runbook.com/fakeurl'
              terms = @{
                priority = 'CRITICAL'
                threshold = 120
                thresholdDuration = 120
              }
              nrql = @{
                query = 'SELECT count(*) FROM AwsLambdaInvocation'
              }
            }
          )
        }
      )
    }
  )
}