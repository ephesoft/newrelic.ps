Function Get-SyntheticMonitorTestCases {
  Return @(
    @{
      propertyName = 'Frequency'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 60
        uri = 'https://some.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
      }
    },
    @{
      propertyName = 'Uri'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some1.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
      }
    },
    @{
      propertyName = 'Locations'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some.net'
        locations = @('AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
      }
    },
    @{
      propertyName = 'Status'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'MUTED'
        slaThreshold = 7
      }
    },
    @{
      propertyName = 'SLAThreshold'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 10
      }
    },
    @{
      propertyName = 'TreatRedirectAsFailure'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
        treatRedirectAsFailure = $true
      }
    },
    @{
      propertyName = 'VerifySSL'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
        VerifySSL = $true
      }
    },
    @{
      propertyName = 'BypassHEADRequest'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
        BypassHEADRequest = $false
      }
    },
    @{
      propertyName = 'ValidationString'
      definedMonitors = @{
        name = 'Test1'
        type = 'SIMPLE'
        frequency = 30
        uri = 'https://some.net'
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
        ValidationString = '200 Okay'
      }
    },
    @{
      propertyName = 'ScriptPath'
      definedMonitors = @{
        name = 'Test1'
        type = 'SCRIPT_API'
        frequency = 30
        locations = @('AWS_CA_CENTRAL_1','AWS_US_EAST_1','AWS_US_EAST_2','AWS_US_WEST_1','AWS_US_WEST_2')
        status = 'ENABLED'
        slaThreshold = 7
        scriptPath = '.\tests\resources\SyntheticMonitor.Script.js'
      }
    }
  )
}