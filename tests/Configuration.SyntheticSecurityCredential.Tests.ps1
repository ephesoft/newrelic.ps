BeforeAll {
  Import-Module '.\NewRelicPS.Configuration.SyntheticSecureCredential.psm1' -Force
  Import-Module '.\NewRelicPS.SyntheticSecureCredentials.psm1' -Force
  $apiKey = 'FAKE-KEY'
}

Describe 'Synthetic Security Credential Configuration' {
  Context 'Set-NRSyntheticSecureCredentialConfiguration' {
    BeforeAll {
      Mock New-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' {}
      Mock Remove-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' {}
      Mock Update-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' {}
      Mock Get-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' {
        @(
          @{
            key = 'TEST1'
            description = 'This is a fake credential'
          },
          @{
            key = 'TEST2'
            description = 'This is a fake credential'
          }
        )
      }
    }
    It 'Updates all defined properties when an existing security credential is found' {
      $definedSecureCredentials = @(
        @{
          key = 'TEST1'
          value = 'Fake-Secret'
          description = 'This is a fake credential'
        },
        @{
          key = 'TEST2'
          value = 'Fake-Secret'
          description = 'This is a fake credential'
        }
      )

      Set-NRSyntheticSecureCredentialConfiguration -AdminAPIKey $apiKey -DefinedSyntheticSecureCredentials $definedSecureCredentials
      Assert-MockCalled New-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 0
      Assert-MockCalled Remove-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 0
      Assert-MockCalled Update-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 2 -ParameterFilter {
        $Value -eq 'Fake-Secret' -and $Description -eq 'This is a fake credential'
      }


    }
    It 'Removes secure credentials that are not defined' {
      $definedSecureCredentials = @(
        @{
          key = 'TEST1'
          value = 'Fake-Secret'
          description = 'This is a fake credential'
        }
      )
      Set-NRSyntheticSecureCredentialConfiguration -AdminAPIKey $apiKey -DefinedSyntheticSecureCredentials $definedSecureCredentials
      Assert-MockCalled New-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 0
      Assert-MockCalled Update-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 1 -ParameterFilter {
        $Key -eq 'TEST1'
      }
      Assert-MockCalled Remove-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 1 -ParameterFilter {
        $Key -eq 'TEST2'
      }
    }

    It 'Creates a new secure credental when no existing secure credential matches the defined credential key' {
      $definedSecureCredentials = @(
        @{
          key = 'TEST1'
          value = 'Fake-Secret'
          description = 'This is a fake credential'
        },
        @{
          key = 'TEST2'
          value = 'Fake-Secret'
          description = 'This is a fake credential'
        },
        @{
          key = 'TEST3'
          value = 'Fake-Secret'
          description = 'This is a fake credential'
        }
      )

      Set-NRSyntheticSecureCredentialConfiguration -AdminAPIKey $apiKey -DefinedSyntheticSecureCredentials $definedSecureCredentials
      Assert-MockCalled New-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 1 -ParameterFilter {
        $Key -eq 'Test3'
      }
      Assert-MockCalled Update-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 2 -ParameterFilter {
        $Key -eq 'TEST1' -or $Key -eq 'Test2'
      }
      Assert-MockCalled Remove-NRSyntheticSecureCredential -ModuleName 'NewRelicPS.Configuration.SyntheticSecureCredential' -Exactly 0
    }
  }
}