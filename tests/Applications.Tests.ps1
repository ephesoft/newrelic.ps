BeforeAll {
  Import-Module .\NewRelicPS.Applications.psm1
}

Describe "Get-NRApplication" {
  Context "When ApplicationId is passed" {
    BeforeAll {
      $applications = [PSCustomObject]@{
        applications = @(
          [PSCustomObject]@{id = "123456789"; name = "TC-CANADA-TST-DEV" }
        )
      }

      Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications {
        return $applications
      }
    }

    It "Should return single application when Application Id is passed" {
      $application = Get-NRApplication -ApplicationID '123456789' -APIKey 'fake-api-key'
            ($application.applications).Count | Should -Be 1
    }

    It "Should return a correct application name" {
      $application = Get-NRApplication -ApplicationID '123456789' -APIKey 'fake-api-key'
      $application.applications.name | Should -Be 'TC-CANADA-TST-DEV'
    }

    It "Should have application id consisting of only numbers" {
      $application = Get-NRApplication -ApplicationID '123456789' -APIKey 'fake-api-key'
      $application.applications.id | Should -Match "^\d{9}$"
    }
  }

  Context "When no ApplicationId is passed" {
    BeforeEach {
      $applications = [PSCustomObject]@{
        applications = @(
          [PSCustomObject]@{id = "123456789"; name = "TC-CANADA-TST-DEV" },
          [PSCustomObject]@{id = "111111111"; name = "TC-CANADA-TST-PROD" }
        )
      }

      Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications {
        return $applications
      }
    }

    It "Should return more than one application when no Application Id is passed" {
      $application = Get-NRApplication -APIKey 'fake-api-key'
            ($application.applications).Count | Should -Be 2
    }

    It "Should return a correct application name" {
      $application = Get-NRApplication -APIKey 'fake-api-key'
            ($application.applications[1]).name | Should -Be 'TC-CANADA-TST-PROD'
    }

    It "Should have application id consisting of only numbers" {
      $application = Get-NRApplication -APIKey 'fake-api-key'
            ($application.applications[1]).id | Should -Match "^\d{9}$"
    }
  }
}

Describe "Get-NRHostName" {
  BeforeAll {
    $application_hosts = [PSCustomObject]@{
      "data" = @{
        "actor" = @{
          "account" = @{
            "id"   = "11111111";
            "nrql" = @{
              "results" = @(
                [PSCustomObject]@{
                  "host_names" = @("TST-DEV-001")
                }
              )
            }
          }
        }
      }
    }

    Mock -CommandName Invoke-NRQLQuery -ModuleName NewRelicPS.Applications {
      return $application_hosts
    }
  }

  It "Should return correct host name" {
    $hostName = Get-NRHostName -APIKey 'fake-api-key' -AccountId 12345678 -ApplicationID 111111111
    $hostName | Should -Be "TST-DEV-001"
  }
}

Describe 'Get-NRAPMEntity' {
  BeforeAll {
    Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications {}
  }

  It 'Should return all entities of specified type when name is not provided' {
    Get-NRAPMEntity -ApiKey 'fake-api-key' -AccountId '123456789'
    Assert-MockCalled -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications -Exactly 1 -ParameterFilter {
      ($body | ConvertFrom-Json).query -like "*name: `"`"*"
    }
  }

  It 'Should return a single entity when a name is provided' {
    Get-NRAPMEntity -ApiKey 'fake-api-key' -AccountId '123456789' -Name 'testing123456789'
    Assert-MockCalled -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications -Exactly 1 -ParameterFilter {
      ($body | ConvertFrom-Json).query -like "*name: testing123456789*"
    }
  }
}

Describe 'Get-NRNormalizationRule' {
  BeforeAll {
    Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications {}
    Mock -CommandName Get-GraphQLQueryGetMetricNormalizationRule -ModuleName NewRelicPS.Applications {}
    Mock -CommandName Get-GraphQLQueryGetMetricNormalizationRuleList -ModuleName NewRelicPS.Applications {}
  }
  It 'Should return a single rule when an Id is provided' {
    Get-NRNormalizationRule -APIKey 'fake-api-key' -AccountId '123456789' -Id '12345'
    Assert-MockCalled -CommandName Get-GraphQLQueryGetMetricNormalizationRule -ModuleName NewRelicPS.Applications -Exactly 1
  }
  It 'Should return a list of all rules when an id is not provided' {
    Get-NRNormalizationRule -APIKey 'fake-api-key' -AccountId '123456789'
    Assert-MockCalled -CommandName Get-GraphQLQueryGetMetricNormalizationRuleList -ModuleName NewRelicPS.Applications -Exactly 1
  }
}

Describe 'New-NRNormalizationRule' {
  BeforeAll {
    Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications {}
  }
  It 'Correctly sends request for resource creation' {
    $Params = @{
      ApiKey                = 'fake-api-key'
      ApplicationGuid       = 'ABC123'
      AccountId             = '123456789'
      Action                = 'Ignore'
      Expression            = '^test/.*'
      Enabled               = $true
      Notes                 = 'testingnotes123'
      Order                 = 12345
      ReplacementExpression = '^test/(1)'
      TerminateChain        = $false
    }
    New-NRNormalizationRule @Params
    Assert-MockCalled -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications -Exactly 1 -ParameterFilter {
      $query = ($body | ConvertFrom-Json).query
      $query -like "*applicationGuid: `"ABC123`"*" -and
      $query -like "*accountId: 123456789*" -and
      $query -like "*matchExpression: `"^test/.*" -and
      $query -like "*enabled: true*" -and
      $query -like "*notes: `"testingnotes123`"*" -and
      $query -like "*evalOrder: 12345*" -and
      $query -like "*replacement: `"^test/(1)`"*" -and
      $query -like "*terminateChain: false*"
    }
  }
}

Describe 'Update-NRNormalizationRule' {
  BeforeAll {
    Mock -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications {}
    Mock -CommandName Get-NRNormalizationRule -ModuleName NewRelicPS.Applications {
      Return [pscustomObject] @{
        action          = 'IGNORE'
        applicationGuid = 'ABC123'
        applicationName = 'TestApp'
        evalOrder       = 10
        enabled         = $false
        id              = 12345
        matchExpression = '^test/.*'
        notes           = 'Test notes'
        replacement     = '^test/(1)'
        terminateChain  = $true
      }
    }
  }
  It 'Uses existing data for non-supplied fields' {
    $Params = @{
      ApiKey    = 'fake-api-key'
      AccountId = '123456789'
      Id        = '123'
    }
    Update-NRNormalizationRule @params
    Assert-MockCalled -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications -Exactly 1 -ParameterFilter {
      $query = ($body | ConvertFrom-Json).query
      $query -like "*id: 123*" -and
      $query -like "*action: IGNORE*"
      $query -like "*accountId: 123456789*" -and
      $query -like "*matchExpression: `"^test/.*" -and
      $query -like "*enabled: false*" -and
      $query -like "*notes: `"Test notes`"*" -and
      $query -like "*evalOrder: 10*" -and
      $query -like "*replacement: `"^test/(1)`"*" -and
      $query -like "*terminateChain: true*"
    }
  }

  It 'Uses data from parameters when supplied' {
    $Params = @{
      ApiKey         = 'fake-api-key'
      AccountId      = '123456789'
      Action         = 'REPLACE'
      Id             = '123'
      Expression     = '^Logging/.*'
      Enabled        = $true
      Notes          = 'Testing123'
      Order          = 20
      Replacement    = '^test/(2)'
      TerminateChain = $false
    }
    Update-NRNormalizationRule @params
    Assert-MockCalled -CommandName Invoke-RestMethod -ModuleName NewRelicPS.Applications -Exactly 1 -ParameterFilter {
      $query = ($body | ConvertFrom-Json).query
      $query -like "*id: 123*" -and
      $query -like "*accountId: 123456789*" -and
      $query -like "*action: REPLACE"
      $query -like "*matchExpression: `"^Logging/.*" -and
      $query -like "*enabled: true*" -and
      $query -like "*notes: `"Testing123`"*" -and
      $query -like "*evalOrder: 20*" -and
      $query -like "*replacement: `"^test/(2)`"*" -and
      $query -like "*terminateChain: false*"
    }
  }
}