Describe 'Sync-NRDashboardConfiguration' {
  BeforeAll {
    Import-Module '.\NewRelicPS.Configuration.Dashboard.psm1' -Force
    Import-Module '.\NewRelicPS.Dashboards.psm1' -Force
    $apiKey = 'Fake-Test-Key'
  }

  Context 'Dashboards configured for sync' {
    BeforeAll {
      $DefinedDashboards = @(
        [PSCustomObject] @{
          sourceDashboardId = '1234'
          updateWidgetAccountIds = $true
          destinationAccounts = @('0123456789')
          destinationBackupPath = '.\path\to\backups'
        }
      )
      Mock Copy-NRDashboard -ModuleName 'NewRelicPS.Configuration.Dashboard' {}
      Mock Get-NRDashboard -ModuleName 'NewRelicPS.Configuration.Dashboard' {}
      Mock Get-NRDashboardList -ModuleName 'NewRelicPS.Configuration.Dashboard' {}
    }

    It 'Copies the dashboard when a single source and destination provided' {
      Sync-NRDashboardConfiguration -APIKey $apiKey -DefinedDashboards $DefinedDashboards
      Assert-MockCalled Copy-NRDashboard -ModuleName 'NewRelicPS.Configuration.Dashboard' -Exactly 1 -ParameterFilter {
        $DashboardId -eq '1234' -and $DestinationAccountId -eq '0123456789'
      }
    }

    It 'Updates widget account Ids when specified' {
      Sync-NRDashboardConfiguration -APIKey $apiKey -DefinedDashboards $DefinedDashboards
      Assert-MockCalled Copy-NRDashboard -ModuleName 'NewRelicPS.Configuration.Dashboard' -Exactly 1 -ParameterFilter {
        $UpdateWidgetAccountIds -eq $true
      }
    }

    It 'Syncs the dashboard to all specified destination accounts' {
      $DefinedDashboards[0].destinationAccounts += ('9876543210')
      Sync-NRDashboardConfiguration -APIKey $apiKey -DefinedDashboards $DefinedDashboards
      Assert-MockCalled Copy-NRDashboard -ModuleName 'NewRelicPS.Configuration.Dashboard' -Exactly 2 -ParameterFilter {
        $DashboardId -eq '1234' -and ($DestinationAccountId -eq '0123456789' -or $DestinationAccountId -eq '9876543210')
      }
    }

    It 'Syncs all specified source dashboards' {
      $DefinedDashboards += [PSCustomObject] @{
        sourceDashboardId = '5678'
        updateWidgetAccountIds = $true
        destinationAccounts = @('0123456789')
      }
      Sync-NRDashboardConfiguration -APIKey $apiKey -DefinedDashboards $DefinedDashboards
      Assert-MockCalled Copy-NRDashboard -ModuleName 'NewRelicPS.Configuration.Dashboard' -Exactly 2 -ParameterFilter {
        ($DashboardId -eq '1234' -or $DashboardId -eq '5678') -and $DestinationAccountId -eq '0123456789'
      }
    }

    It 'Backs up configuration before overwrite' {

      Mock Get-NRDashboardList -ModuleName 'NewRelicPS.Configuration.Dashboard' {
        Return @{
          Name = 'fake1'
          AccountId = '0123456789'
          GUID="FakeGuid1234"
        }
      }
      Mock Get-NRDashboard -ModuleName 'NewRelicPS.Configuration.Dashboard' {
        Return @{
          Name = 'Fake1'
          Account = '0123456789'
          Random = (new-guid).guid
        }
      }
      Mock Out-File -ModuleName 'NewRelicPS.Configuration.Dashboard' {}

      Sync-NRDashboardConfiguration -APIKey $apiKey -DefinedDashboards $DefinedDashboards
      Assert-MockCalled Out-File -ModuleName 'NewRelicPS.Configuration.Dashboard' -Exactly 1
    }
  }
}
