Describe 'Set-NRChannelPolicyLink' {
  BeforeAll {
    Import-Module '.\NewRelicPS.Configuration.Channel.psm1' -Force
    $apiKey = 'Fake-Test-Key'
    Mock Get-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Channel' {
      @(
        @{
          id = '1231'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test1'
        },
        @{
          id = '1232'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test2'
        },
        @{
          id = '1233'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test3'
        }
      )
    }

    Mock Get-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' {
      @(
        [pscustomobject]@{
          id = '4561'
          name = 'TestChannel1'
          type = 'user'
          configuration = @{
            user_id = '12345678'
          }
          links = @{
            policy_ids = $null
          }
        },
        [pscustomobject]@{
          id = '4562'
          name = 'TestChannel2'
          type = 'opsgenie'
          configuration = @{
            tags = @('Sandbox', 'HighUrgency')
          }
          links = @{
            policy_ids = '1231'
          }
        },
        [pscustomobject]@{
          id = '4563'
          name = 'TestChannel3'
          type = 'opsgenie'
          configuration = @{
            tags = @('Sandbox', 'MediumUrgency')
          }
          links = @{
            policy_ids = @('1232','1233')
          }
        }
      )
    }
    Mock Add-NRNotificationChannelToAlertPolicy -ModuleName 'NewRelicPS.Configuration.Channel' {}
    Mock Remove-NRNotificationChannelFromAlertPolicy -ModuleName 'NewRelicPS.Configuration.Channel' {}

    # Set default policies declaration for testing
    $policies = @(
      @{
        name = 'Test1'
        incident_preference = 'PER_CONDITION_AND_TARGET'
        channels = 'TestChannel2'
      },
      @{
        name = 'Test2'
        incident_preference = 'PER_CONDITION_AND_TARGET'
        channels = 'TestChannel3'
      },
      @{
        name = 'Test3'
        incident_preference = 'PER_CONDITION_AND_TARGET'
        channels = 'TestChannel3'
      }
    )
  }
  It 'Creates a defined channel policy link when one does not exist' {
    $policies = @(
      @{
        name = 'Test1'
        incident_preference = 'PER_CONDITION_AND_TARGET'
        channels = @('TestChannel1','TestChannel2')
      },
      @{
        name = 'Test2'
        incident_preference = 'PER_CONDITION_AND_TARGET'
        channels = 'TestChannel3'
      },
      @{
        name = 'Test3'
        incident_preference = 'PER_CONDITION_AND_TARGET'
        channels = 'TestChannel3'
      }
    )
    Set-NRChannelPolicyLink -APIKey $apiKey -DefinedPolicies $policies -WarningAction 'SilentlyContinue'

    # The parameter filter here is using magic string because you can't use variables inside the mock
    Assert-MockCalled Add-NRNotificationChannelToAlertPolicy -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 1 -ParameterFilter {
      $PolicyId -eq '1231' -and $ChannelIds -eq '4561'
    }
  }

  It 'Does nothing when the link exists already' {

    Set-NRChannelPolicyLink -APIKey $apiKey -DefinedPolicies $policies
    Assert-MockCalled Add-NRNotificationChannelToAlertPolicy -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 0
    Assert-MockCalled Remove-NRNotificationChannelFromAlertPolicy -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 0
  }

  It 'Removes existing links not specified in the definition' {

    # Mock channels and add extra policy link on TestChannel1
    Mock Get-NRNotificationChannel -ModuleName 'NewRelicPS.Configuration.Channel' {
      @(
        [pscustomobject]@{
          id = '4561'
          name = 'TestChannel1'
          type = 'user'
          configuration = @{
            user_id = '12345678'
          }
          links = @{
            policy_ids = '1231'
          }
        },
        [pscustomobject]@{
          id = '4562'
          name = 'TestChannel2'
          type = 'opsgenie'
          configuration = @{
            tags = @('Sandbox', 'HighUrgency')
          }
          links = @{
            policy_ids = '1231'
          }
        },
        [pscustomobject]@{
          id = '4563'
          name = 'TestChannel3'
          type = 'opsgenie'
          configuration = @{
            tags = @('Sandbox', 'MediumUrgency')
          }
          links = @{
            policy_ids = @('1232','1233')
          }
        }
      )
    }

    Set-NRChannelPolicyLink -APIKey $apiKey -DefinedPolicies $policies -WarningAction 'SilentlyContinue'
    Assert-MockCalled Remove-NRNotificationChannelFromAlertPolicy -ModuleName 'NewRelicPS.Configuration.Channel' -Exactly 1 -ParameterFilter {
      $PolicyId -eq '1231' -and $ChannelId -eq '4561'
    }
  }
}
