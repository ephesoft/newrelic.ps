// Script used for unit tests
// Import the 'assert' module to validate results.
var assert = require('assert');
var options = {
  //Define endpoint URL.
  url: `https://api.${ENVIRONMENT_DOMAIN_API}/v1/auth`,
  //Define body of POST request.
  body: `{"tenantDomain":\"${HEALTH_TENANT_NAME}\","email":\"${HEALTH_TENANT_USERNAME}\","password":\"${$secure.HEALTH_TENANT_PASSWORD}\"}`,
  headers: {
    'Content-Type': 'application/json'
  }
};
// Define expected results using callback function.
function callback(error, response, body) {
  // Log status code to Synthetics console.
  console.log(response.statusCode + " status code")
  // Verify endpoint returns 200 (OK) response code.
  assert.ok(response.statusCode == 200, 'Expected 200 OK response');
  // Parse JSON received from Insights into variable.
  var info = JSON.parse(body);
  // Verify that `info` contains element named data.token.
  assert(info.data.token, 'Expected to geta token back from the service');
  // Log end of script.
  console.log("End reached");
}
// Make POST request, passing in options and callback.
$http.post(options, callback);