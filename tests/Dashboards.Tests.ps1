Describe 'Copy-NRDashboard' {
  BeforeAll {
    Import-Module '.\NewRelicPS.Dashboards.psm1' -Force
    Import-Module '.\NewRelicPS.GraphQLQueries.psm1' -Force

    $fakeAccountId = '1234'
    $fakeAPIKey = 'Fake-Test-Key'
    $fakeDashboardId = 'ImaFakeGUID'
    $fakeDashboardConfig = [PSCustomObject] @{
      fake = 'fakedata'
    }

    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.Dashboards' {}
    Mock Update-NRDashboardWidgetAccountID -ModuleName 'NewRelicPS.Dashboards' {
      Return [PSCustomObject] @{
        name      = 'fakeDashboard'
        guid      = 'ImaFakeGUID'
        accountId = '1234'
      }
    }
    Mock Get-NRDashboardList -ModuleName 'NewRelicPS.Dashboards' {
      Return [PSCustomObject] @{
        name      = 'fakeDashboard'
        guid      = 'ImaFakeGUID'
        accountId = '1234'
      }
    }
    Mock New-NRDashboard -ModuleName 'NewRelicPS.Dashboards' {
      Return [PSCustomObject] @{
        entityresult = @{
          name = 'fakeDashboard'
          guid = 'ImaFakeGUID'
        }
      }
    }
    Mock Get-NRDashboard -ModuleName 'NewRelicPS.Dashboards' {
      Return [PSCustomObject] @{
        name = 'fakeDashboard'
      }
    }
    Mock Update-NRDashboard -ModuleName 'NewRelicPS.Dashboards' {}
  }

  It 'Replaces widget account Ids when specified' {
    Copy-NRDashboard -APIKey $fakeAPIKey -DashboardId $fakeDashboardId -DestinationAccountId $fakeAccountId -UpdateWidgetAccountIds
    Assert-MockCalled Update-NRDashboardWidgetAccountID -ModuleName 'NewRelicPS.Dashboards' -Exactly 1 -ParameterFilter {
      $AccountId -eq $fakeAccountId
    }
  }

  It 'Does not replace widget account Ids unless specified' {
    Copy-NRDashboard -APIKey $fakeAPIKey -DashboardId $fakeDashboardId -DestinationAccountId $fakeAccountId
    Assert-MockCalled Update-NRDashboardWidgetAccountID -ModuleName 'NewRelicPS.Dashboards' -Exactly 0
  }

  It 'Creates a new dashboard if one does not already exist' {
    Mock Get-NRDashboard -ModuleName 'NewRelicPS.Dashboards' {
      Return [PSCustomObject] @{
        name      = 'fakeDashboard2'
        guid      = 'ImaAnotherFakeGUID'
        accountId = '1234'
      }
    }
    Copy-NRDashboard -APIKey $fakeAPIKey -DashboardId $fakeDashboardId -DestinationAccountId $fakeAccountId
    Assert-MockCalled New-NRDashboard -ModuleName 'NewRelicPS.Dashboards' -Exactly 1 -ParameterFilter {
      $AccountId -eq $fakeaccountId
    }
  }

  It 'Updates an existing dashboard' {
    Copy-NRDashboard -APIKey $fakeapikey -DashboardId $fakeDashboardId -DestinationAccountId $fakeAccountId
    Assert-MockCalled Update-NRDashboard -ModuleName 'NewRelicPS.Dashboards' -Exactly 1 -ParameterFilter {
      $DashboardId -eq $fakeDashboardId
    }
  }

  It 'Throws an error if there are multiple destination dashboards with the same name in the same account' {
    Mock Get-NRDashboardList -ModuleName 'NewRelicPS.Dashboards' {
      Return (
        [PSCustomObject] @{
          name      = 'fakeDashboard'
          guid      = 'ImaFakeGUID'
          accountId = '1234'
        },
        [PSCustomObject] @{
          name      = 'fakeDashboard'
          guid      = 'ImaFakeGUID2'
          accountId = '1234'
        }
      )
    }

    # Run the command and expect an error to be thrown
    { Copy-NRDashboard -APIKey $fakeapikey -DashboardId $fakeDashboardId -DestinationAccountId $fakeAccountId } | Should -Throw
  }
}

Describe 'Get-NRDashboardList' {
  BeforeAll {
    $hash = @{
      Counter = 1
    }
    Import-Module '.\NewRelicPS.Dashboards.psm1' -Force
    Import-Module '.\NewRelicPS.GraphQLQueries.psm1' -Force

    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.Dashboards' {
      $nextCursor = 'abcd'
      If ($hash.Counter -ge 3) {
        $nextCursor = ''
      }
      $hash.Counter++
      Return [PSCustomObject] @{
        data = @{
          actor = @{
            entitysearch = @{
              results = @{
                nextCursor = $nextCursor
                entities   = @(@{
                    name      = "fakeDashboard"
                    guid      = "ImaFakeGUID"
                    accountId = '1234'
                  })
              }
            }
          }
        }
      }
    }
  }
  It 'Returns all dashboards when list is paginated' {
    Get-NRDashboardList -APIKey 'Fake-API-Key'
    Assert-MockCalled Invoke-RestMethod -ModuleName 'NewRelicPS.Dashboards' -Exactly 3
  }
}