Describe 'Set-NRPolicyConfiguration' {
  BeforeAll {
    Import-Module '.\NewRelicPS.Configuration.Policy.psm1' -Force
    $apiKey = 'Fake-Test-Key'
    Mock Get-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' {
      @(
        @{
          id = '123451'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test1'
        },
        @{
          id = '123452'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test2'
        },
        @{
          id = '123453'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test3'
        }
      )
    }
    Mock New-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' {}
    Mock Update-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' {}
    Mock Remove-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' {}

  }

  It 'Successfully handles a single existing alert policy' {

    # Mock a single existing policy
    Mock Get-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' {
      @(
        @{
          id = '123451'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test1'
        }
      )
    }

    $policy = @{
      name = 'Test1'
      incident_preference = 'PER_CONDITION'
    }

    Set-NRPolicyConfiguration -APIKey $apiKey -DefinedPolicies $policy -WarningAction 'SilentlyContinue'

    Assert-MockCalled Update-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 1 -ParameterFilter {
      $Name -eq 'Test1' -and $IncidentPreference -eq 'PER_CONDITION'
    }
    Assert-MockCalled New-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
    Assert-MockCalled Remove-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
  }

  It 'Creates a defined policy when it does not already exist' {
    $policy = @{
      name = 'TestPolicy'
      incident_preference = 'PER_CONDITION'
    }

    # Mock Compare-Object so that the CMDLet does not throw due to the policy not actually being created
    Mock Compare-Object -ModuleName 'NewRelicPS.Configuration.Policy' {}
    Set-NRPolicyConfiguration -APIKey $apiKey -DefinedPolicies $policy -WarningAction 'SilentlyContinue'

    Assert-MockCalled New-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -ParameterFilter {
      $Name -eq $policy.name -and $IncidentPreference -eq $policy.incident_preference
    }
  }

  It 'Does nothing when all policies exists and all properties match' {
    $policies = @(
      @{
        name = 'Test1'
        incident_preference = 'PER_CONDITION_AND_TARGET'
      },
      @{
        name = 'Test2'
        incident_preference = 'PER_CONDITION_AND_TARGET'
      },
      @{
        name = 'Test3'
        incident_preference = 'PER_CONDITION_AND_TARGET'
      }
    )

    Set-NRPolicyConfiguration -APIKey $apiKey -DefinedPolicies $policies
    Assert-MockCalled New-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
    Assert-MockCalled Update-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
    Assert-MockCalled Remove-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
  }

  It 'Updates a policy when the incident preference does not match' {
    $policies = @(
      @{
        name = 'Test1'
        incident_preference = 'PER_CONDITION'
      },
      @{
        name = 'Test2'
        incident_preference = 'PER_CONDITION_AND_TARGET'
      },
      @{
        name = 'Test3'
        incident_preference = 'PER_CONDITION_AND_TARGET'
      }
    )
    Set-NRPolicyConfiguration -APIKey $apiKey -DefinedPolicies $policies
    Assert-MockCalled New-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
    Assert-MockCalled Update-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 1 -ParameterFilter {
      $Name -eq $policies[0].name -and $IncidentPreference -eq $policies[0].incident_preference
    }
    Assert-MockCalled Remove-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
  }

  It 'Removes an existing policy that is not defined' {
    $policies = @(
      @{
        name = 'Test2'
        incident_preference = 'PER_CONDITION_AND_TARGET'
      },
      @{
        name = 'Test3'
        incident_preference = 'PER_CONDITION_AND_TARGET'
      }
    )
    Set-NRPolicyConfiguration -APIKey $apiKey -DefinedPolicies $policies -WarningAction 'SilentlyContinue'
    Assert-MockCalled New-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
    Assert-MockCalled Update-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 0
    Assert-MockCalled Remove-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.Policy' -Exactly 1 -ParameterFilter {
      $PolicyId -eq '123451'
    }
  }
}
