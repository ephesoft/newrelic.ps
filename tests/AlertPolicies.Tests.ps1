BeforeAll {
  Import-Module '.\NewRelicPS.AlertPolicies.psm1' -Force
}

Describe 'Alert Policy CMDLets' {
  BeforeAll {
    Mock Invoke-RestMethod -ModuleName 'NewRelicPS.AlertPolicies' {
      Return @{
        policies = @(
          @{
            name = 'one'
          },
          @{
            name = 'two'
          }
        )
      }
    }
  }

  It 'Get-NRAlertPolicy: Returns all results when name parameter not given' {
    $Result = Get-NRAlertPolicy -APIKey 'Fake-API-Key'
    $Result.count | Should -Be 2
  }

  It 'Get-AlertPolicy: Returns only the requested item when name parameter given' {
    $Result = Get-NRAlertPolicy -APIKey 'Fake-API-Key' -Name 'one'
    $Result.count | Should -Be 1
    $Result.name | Should -Be 'one'
  }
}
