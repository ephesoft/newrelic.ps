. '.\tests\testCases\SyntheticLocationConditions.ps1'

BeforeAll {
  Import-Module '.\NewRelicPS.SyntheticMonitors.psm1' -Force
  Import-Module '.\NewRelicPS.SyntheticLocationConditions.psm1' -Force
}

Describe 'Set-NRSyntheticLocationConditionConfiguration' {
  $testCases = Get-SyntheticLocationConditionTestCases

  BeforeAll {
    Import-Module '.\NewRelicPS.Configuration.SyntheticLocationCondition.psm1' -Force
    $apiKey = 'Fake-Test-Key'
    $policyId = '1234'
    $name = 'Test1'
    $type = 'SyntheticLocation'
    $entities = @('Monitor1')
    $criticalTerm = @{
      priority = 'critical'
      threshold = 5
    }
    $warningTerm = @{
      priority = 'warning'
      threshold = 2
    }
    Mock Get-NRSyntheticMonitor  -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' {
      @(
        @{
          id = 1234
          name = 'Monitor1'
        },
        @{
          id = 5678
          name = 'Monitor2'
        }
      )
    }
    Mock Get-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' {
      @(
        @{
          id = 67891
          name = 'TestCondition1'
          enabled = $true
          terms = @{
            priority = 'critical'
            threshold = 5
          }
          type = 'SyntheticLocation'
          entities = @('1234')
        }
      )
    }
    Mock Get-NRAlertPolicy -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' {
      @(
        @{
          id = '123451'
          incident_preference = 'PER_CONDITION_AND_TARGET'
          name = 'Test1'
        }
      )
    }
    Mock New-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' {}
    Mock Update-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' {}
    Mock Remove-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' {}
  }
  It 'Creates a defined condition when one does not exist' {
    $definedPolicies = @(
      @{
        Name = $Name
        Conditions = @(
          @{
            type = $type
            name = $Name
            enabled = $true
            entities = $entities
            terms = @( $criticalTerm )
          }
        )
      }
    )

    Set-NRSyntheticLocationConditionConfiguration -AdminAPIKey $apiKey -DefinedPolicies $definedPolicies
    Assert-MockCalled Update-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 0
    Assert-MockCalled New-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 1 -ParameterFilter {
      $Name -eq $definedPolicies[0].Name
    }
  }

  It 'Does nothing when defined conditions exactly match existing conditions on all policies' {

    # Set the defined policy to match what is mocked
    $definedPolicies = @(
      @{
        Name = $Name
        Conditions = @(
          @{
            name = 'TestCondition1'
            terms = @{
              priority = 'critical'
              threshold = 5
            }
            type = 'SyntheticLocation'
            entities = @('Monitor1')
          }
        )
      }
    )

    Set-NRSyntheticLocationConditionConfiguration -AdminAPIKey $apiKey -DefinedPolicies $definedPolicies
    Assert-MockCalled New-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 0
    Assert-MockCalled Update-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 0
    Assert-MockCalled Remove-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 0
  }

  It 'Updates an existing condition when defined property <propertyName> does not match' -TestCases $TestCases {

    # Return a condition with multiple entities for this test
    Mock Get-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' {
      @(
        @{
          id = 67891
          name = 'TestCondition1'
          enabled = $true
          terms = @{
            priority = 'critical'
            threshold = 5
          }
          type = 'SyntheticLocation'
          entities = @('1234', '5678')
        }
      )
    }

    Set-NRSyntheticLocationConditionConfiguration -AdminAPIKey $apiKey -DefinedPolicies $definedPolicies -WarningAction 'SilentlyContinue'
    Assert-MockCalled Update-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 1  -ParameterFilter {
      $Id -eq '67891' -and (Get-Variable -Name $propertyName).value -eq $expectedValue
    }
  }

  It 'Removes existing conditions when they are not defined' {

    # This defined policy doesn't have any conditions so the Get-NRSyntheticLocationCondition mock will return one which should be removed
    $definedPolicies = @(
      @{
        Name = 'Test1'
        Conditions = @()
      }
    )

    Set-NRSyntheticLocationConditionConfiguration -AdminAPIKey $apiKey -DefinedPolicies $definedPolicies -WarningAction 'SilentlyContinue'
    Assert-MockCalled New-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 0
    Assert-MockCalled Update-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 0
    Assert-MockCalled Remove-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 1 -ParameterFilter {
      $Id -eq '67891'
    }
  }
  It 'Handles null entity lists' {

    # Set the defined policy to have a condition with an empty entities list
    $definedPolicies = @(
      @{
        Name = $Name
        Conditions = @(
          @{
            name = 'TestCondition1'
            terms = @{
              priority = 'critical'
              threshold = 5
            }
            type = 'SyntheticLocation'
            entities = @()
          }
        )
      }
    )

    Set-NRSyntheticLocationConditionConfiguration -AdminAPIKey $apiKey -DefinedPolicies $definedPolicies
    Assert-MockCalled Update-NRSyntheticLocationCondition -ModuleName 'NewRelicPS.Configuration.SyntheticLocationCondition' -Exactly 1
  }
}