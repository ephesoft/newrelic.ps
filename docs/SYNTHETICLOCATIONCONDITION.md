# ConditionConfiguration
Defines a condition on an alert policy.  Conditions are used to evaluate a signal and create violations when a defined threshold is crossed.

```yml
  conditions:
    - name: String
      type: String
      enabled: Boolean
      entities: SyntheticLocationConditionEntities
      terms: SyntheticLocationConditionTerms
      runbook_url: String
      violation_time_limit_seconds: Number
```

## **enabled**
---
  Specifies whether the condition should be created as enabled.  The condition defaults to true if not provided.

  Required: No

  Type: Boolean

## **entities**
---
  An array of synthetic monitor names to alert on.  The array must contain at least one name.

  Required: Yes

  Type: Array

## **name**
---
  The name given to the defined condition.

  Required: Yes

  Type: String

## **runbook_url**
---
  A URL to display in notificaitons, most often used to link to a runbook to be executed when a notification is recieved.

  Required: No

  Type: String

## **terms**
---
  Terms define the properties around the condition threshold. A condition must have a term with a critical priority defined.  An additional term with a warning priority may also be defined and is optional.

  Required: Yes

  Type: [ConditionTerms](SYNTHETICLOCATIONCONDITIONTERMS.md)

## **type**
---
  Identifies the condition as a SyntheticLocation type.  The only valid option is SyntheticLocation.

  Required: Yes

  Type: String

## **violation_time_limit_seconds**
---
 The maximum number of seconds a violation can remain open before being closed by the sytem.

 Required: No

 Type: Number

## Example

```yml
AlertPolicies:
  - name: Example1
    incident_preference: PER_CONDITION_AND_TARGET
    conditions:
      - name: MyExampleCondition
        type: 'SyntheticLocation'
        enabled: true
        terms:
          - priority: critical
            threshold: 2
          - priority: warning
            threshold: 1
        entities:
          - Test1
          - Test2
        runbook_url: 'https://some.net'
        violation_time_limit_seconds: 86400
```