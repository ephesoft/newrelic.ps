# Parameters
Sensitive configuration items can be passed as a parameter at run time rather than being stored insecurely in the yml configuration file.

## Defining a Parameter
Use this syntax to define a parameter: `${MySecret}`

The below example uses a parameter for the api_key property.  The real value is later passed into the configuration in at run time.

### Example
```yml
NotificationChannels:
  - name: OpsGenie-Sandbox-HighUrgency
    type: OpsGenie
    configuration:
      api_key: ${OpsGenieAPIKey}
      tags:
        - Sandbox
        - HighUrgency
  - name: OpsGenie-Sandbox-MediumUrgency
    type: OpsGenie
    configuration:
      api_key: ${OpsGenieAPIKey}
      tags:
        - Sandbox
        - MediumUrgency
```

## Passing a Parameter at Runtime
Parameters are passed into the `Update-NRConfiguration` CMDLet at runtime in a hash table like this:
```PowerShell
$Parameters = @{
  MySecret = 'My-Secret-Key'
  OpsGenieAPIKey = 'FAKE-API-Key'
}
Update-NRConfiguration -Parameters $Parameters ...
```