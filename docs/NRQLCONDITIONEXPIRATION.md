# Condition Expiration
Defines how New Relic handles signal loss. Not valid on Outlier conditions.

```yml
expiration:
  closeViolationsOnExpiration: Boolean
  expirationDuration: Number
  openViolationOnExpiration: Boolean
```

## **closeViolationsOnExpiration**
---
Indicates whether open violations should be closed when the signal is lost.

Defaults to true.

Required: No

Type: Boolean

## **expirationDuration**
---
The number of seconds before a signal is considered lost when not reporting in.

Defaults to 600.

Required: No

Type: Number

## **openViolationOnExpiration**
---
Indicates whether a new violation should be created when the signal is lost.

Required: No

Type: Boolean

## Example
```yml
AlertPolicies:
  - name: Sandbox-Critical-HighUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-HighUrgency
    conditions:
      - type: static
        name: test1
        expiration:
          closeViolationsOnExpiration: $false
          expirationDuration: 120
          openViolationOnExpiration: $true
        terms:
          - priority: 'CRITICAL'
            threshold: 120
            thresholdDuration: 60
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
      - type: baseline
        name: baselinetest1
        terms:
          - priority: 'CRITICAL'
            threshold: 110
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
```