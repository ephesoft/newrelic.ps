# Condition Terms
Terms define the properties around the condition threshold. A condition must have a term with a critical priority defined.  An additional term with a warning priority may also be defined and is optional.

```yml
terms:
  operator: String
  priority: String
  threshold: Number
  thresholdDuration: Number
  thresholdOccurrences: Number
```

## **operator**
---
Determines what comparison will be used between the value_function and the terms[threshold] value to trigger an event on the warning threshold.

Default: ABOVE

Required: No

Valid values: ABOVE,BELOW,EQUAL


## **priority**
---
Determines whether the defined threshold should open a new violation and send a notification or not.

There are two types of priorities:
* Critical: Opens a new violation so a notification is sent.  Dashboards will show a red background while this threshold is crossed.
* Warning: Does not open a violation and no notiication is sent.  Dashboards will show a yellow background while thsi threshold is crossed.

Required: Yes

Type: String

Valid values: Critical, Warning


## **threshold**
---
Sets the target value for creating an incident.

Required: Yes

Type: Number

## **thresholdDuration**
---
The time (in seconds) for the condition to persist before triggering an event on the threshold.

Default: 300

Required: No

Type: Number

## **thresholdOccurrences**
---
Determines whether an event is created when any data point within the duration crosses a threshold (AT_LEAST_ONCE) or if ALL data points wthin the duration must cross the threshold to create an event.

Default: ALL

Required: No

Valid values: ALL,AT_LEAST_ONCE

## Example
```yml
AlertPolicies:
  - name: Sandbox-Critical-HighUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-HighUrgency
    conditions:
      - type: static
        name: test1
        terms:
          - priority: 'CRITICAL'
            threshold: 120
            thresholdDuration: 60
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
      - type: baseline
        name: baselinetest1
        terms:
          - priority: 'CRITICAL'
            threshold: 110
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
```