# Dashboards
```yml
Dashboards:
  - sourceDashboardId: String
    destinationBackupPath: String
    destinationAccounts:
      - String
      - String
    updateWidgetAccountIds: Boolean
```
## **destinationAccounts**
---
  A list of accounts where the source dashboard should be sync'd to

  Required: Yes

  Type: List of strings

## **destinationBackupPath**
---
  Path to the folder where backups of dashboards are taken before they are overwritten by the sync process.

  Required: No

  Type: String

## **sourceDashboardId**
---
  The GUID for the dashboard to be sync'd across accounts

  Required: Yes

  Type: String

## **updateWidgetAccountIds**
---
  Defaults to false.  If set to true, all account IDs from the source dashboard are updated to the destination account Id.  This is useful for copying a dashboard across New Relic accounts which represent environments.

  Required: No

  Type: Boolean

## Example
```yml
Dashboards:
  - sourceDashboardId: 'ABCD123456789'
    destinationBackupPath: './path/to/dashboardBackups'
    destinationAccounts:
      - '0123456789'
      - '9876543210'
    updateWidgetAccountIds: true
```
