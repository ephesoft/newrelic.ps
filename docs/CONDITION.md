# ConditionConfiguration
Defines a condition on an alert policy.  Conditions are used to evaluate a signal and create violations when a defined threshold is crossed.

This module supports two types of conditions.  A configuration can have both types of conditions in the same policy.  Use the links below for details on each condition type.

* [NRQL Conditions](NRQLCONDITION.md)
* [Synthetic Location Conditions](SYNTHETICLOCATIONCONDITION.md)

NRQL Condition:
```yml
  conditions:
    - name: String
      type: String
      enabled: Boolean
      baselineDirection: String
      description: String
      expectedGroups: Number
      expiration: ConditionExpiration
      nrql: ConditionNRQL
      openViolationOnGroupOverlap: Boolean
      runbookUrl: String
      terms: NRQLConditionTerms
      valueFunction: String
      violationTimeLimit: Number
```
Synthetic Location Condition:
```yml
  conditions:
    - name: String
      type: String
      enabled: Boolean
      entities: SyntheticLocationConditionEntities
      terms: SyntheticLocationConditionTerms
      runbook_url: String
      violation_time_limit_seconds: Number
```
