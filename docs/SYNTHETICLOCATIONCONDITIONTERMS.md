# Condition Terms
Terms define the properties around the condition threshold. A condition must have a term with a critical priority defined.  An additional term with a warning priority may also be defined and is optional.

```yml
terms:
  priority: String
  threshold: Number
```

## **priority**
---
Determines whether the defined threshold should open a new violation and send a notification or not.

There are two types of priorities:
* Critical: Opens a new violation so a notification is sent.  Dashboards will show a red background while this threshold is crossed.
* Warning: Does not open a violation and no notiication is sent.  Dashboards will show a yellow background while thsi threshold is crossed.

Required: Yes

Type: String

Valid values: Critical, Warning


## **threshold**
---
Sets the target value for creating an incident.  A warning threshold can be removed by setting the warning threshold to 0.

Required: Yes

Type: Number


## Example
```yml
AlertPolicies:
  - name: Example1
    incident_preference: PER_CONDITION_AND_TARGET
    conditions:
      - name: MyExampleCondition
        type: 'SyntheticLocation'
        enabled: true
        terms:
          - priority: critical
            threshold: 2
          - priority: warning
            threshold: 0 # Removes an existing warning threshold
        entities:
          - Test1
          - Test2
        runbook_url: 'https://some.net'
        violation_time_limit_seconds: 86400
```