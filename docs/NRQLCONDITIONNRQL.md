# Condition NRQL

```yml
nrql:
  query: String
  evaluationOffset: Number
```

## **query**
---
  The NRQL query to be used for the condition.

  Required: Yes

  Type: String

## **evaluationOffset**
---
  This is the timeframe (in minutes) in which to evaluate the specified NRQL query.  Valid values: 1-20.

  New Relic recommends evaluating data from three minutes ago or longer. An offset of less than 3 minutes will trigger violations sooner, but you might see more false positives and negatives due to data latency.

  Required: No

  Type: Number

## Example
```yml
AlertPolicies:
  - name: Sandbox-Critical-HighUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-HighUrgency
    conditions:
      - type: static
        name: test1
        terms:
          - priority: 'CRITICAL'
            threshold: 120
            thresholdDuration: 60
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
          evaluationOffset: 3
```