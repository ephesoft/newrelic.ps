# Notification Channel Configuration
See the [New Relic API Explorer Create Channels](https://rpm.newrelic.com/api/explore/alerts_channels/create) page for a list of types and their associated configurations.

The OpsGenie channel type is shown here as an example and is currently the only type tested.  Other types may work but have not be tested.

```yml
configuration:
  api_key: String
  tags: List
```

## **api_key**
---
  The APIKey New Relic uses to send OpsGenie alerts.  It is recommended that you use pass this value as a [parameter](PARAMETERS.md) at runtime rather than store it directly in the yml file.

  Required: Yes

  Type: String

## **tags**
---
  A list of tags to be added onto the notification being sent by this channel

  Required: No

  Type: List

## Example
```yml
NotificationChannels:
  - name: OpsGenie-Sandbox-HighUrgency
    type: OpsGenie
    configuration:
      api_key: ${OpsGenieAPIKey}
      tags:
        - Sandbox
        - HighUrgency
```

