# Building a Configuration File
Use the details here to define a desired configuration in YAML.  The configuration file is then used by `Update-NRConfiguration` to apply the defined configuration to a New Relic account.

[Parameters](PARAMETERS.md) can be passed into the configuration real-time to avoid exposure of sensitive values.

## Alerting Configuration
To define an alerting configuration, specify at least one notification channel and alert policy with a condition.

1. Specify any [Dashboards](DASHBOARD.md) to sync across accounts (optional).
2. Specify any [Synthetic Monitors](SYNTHETICMONITOR.md) being used (optional).
3. Specify at least one [Notification Channel](NOTIFICATIONCHANNEL.md)
4. Specify at least one [Alert Policy](ALERTPOLICY.md)

## Example Configuration File
```yml
Dashboards:
  - sourceDashboardId: 'ABCD123456789'
    destinationBackupPath: './path/to/dashboardBackups'
    destinationAccounts:
      - '0123456789'
      - '9876543210'
    updateWidgetAccountIds: true
SyntheticMonitors:
  - name: Example1
    type: Simple
    frequency: 10
    uri: 'https://some.net'
    locations:
      - AWS_CA_CENTRAL_1
      - AWS_US_EAST_1
      - AWS_US_EAST_2
      - AWS_US_WEST_1
      - AWS_US_WEST_2
    slaThreshold: 7
    status: Enabled

NotificationChannels:
  - name: OpsGenie-Sandbox-HighUrgency
    type: OpsGenie
    configuration:
      api_key: ${OpsGenieAPIKey}
      tags:
        - Sandbox
        - HighUrgency
  - name: OpsGenie-Sandbox-MediumUrgency
    type: OpsGenie
    configuration:
      api_key: ${OpsGenieAPIKey}
      tags:
        - Sandbox
        - MediumUrgency

AlertPolicies:
  - name: Sandbox-Critical-HighUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-HighUrgency
    conditions:
      - type: static
        name: test1
        terms:
          - priority: 'CRITICAL'
            threshold: 100
            thresholdDuration: 60
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
      - type: baseline
        name: baselinetest1
        terms:
          - priority: 'CRITICAL'
            threshold: 120
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
  - name: Sandbox-Critical-MediumUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-MediumUrgency
    conditions:
      - type: baseline
        name: baselinetest2
        terms:
          - priority: 'CRITICAL'
            threshold: 120
            thresholdDuration: 120
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation

      - type: 'SyntheticLocation'
        name: MyExampleCondition
        enabled: true
        terms:
          - priority: critical
            threshold: 2
          - priority: warning
            threshold: 1
        entities:
          - Example1
        runbook_url: 'https://somerunbook.net'
        violation_time_limit_seconds: 86400

  - name: Sandbox-Warning-MediumUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-MediumUrgency



```
