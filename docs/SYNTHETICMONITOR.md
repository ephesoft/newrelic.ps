# SyntheticMonitor
Defines a Synthetic Monitor to proactively monitor the status of a website.

```yml
SyntheticMonitors:
  - name: String
    type: String
    frequency: Number
    uri: String
    locations: Array
    scriptPath: String
    scriptVariables: Dictionary
    slaThreshold: Number
    status: String
    bypassHEADRequest: Boolean
    treatRedirectAsFailure: Boolean
    ValidationString: String
    verifySSL: Boolean
```

## **bypassHEADRequest**
---
  Skips the default HEAD request and instaed uses the GET verb with a ping check.  Defaults to True.  Valid only for SIMPLE monitors.

  Required: No

  Type: Boolean

## **frequency**
---
  Specifies the interval for attempting a connection to the website in minutes.

  Required: yes

  Type: Number

  Valid Values: 1, 5, 10, 15, 30, 60, 360, 720, 1440

## **locations**
---
  An array of locations to probe from.  Get available options by running the Get-NRSyntheticLocation CMDLet.  A

  Required: Yes

  Type: Array

## **name**
---
  The name given to the defined condition.

  Required: Yes

  Type: String

## **scriptPath**
---
  File path to the script used for a scripted monitor.  Valid only for SCRIPT_API and SCRIPT_BROWSER types.

  Note that synthetic scripts support [parameters](docs/PARAMETERS.md).

  Required: Only for SCRIPT_API and SCRIPT_BROWSER types

  Type: String

## **scriptVariables**
---
  A dictionary of variables whose values replace the variable key when the synthetic monitor script is deployed.  Using script variables for non-sensitive data
  allows those items to be output in the monitor logs.  Use synthetic secure variables for sensitive data.

  Required: No

  Type: Dictionary

## **slaThreshold**
---
  The number of seconds for a check to finish witin to be considered satisfying.  Defaults to 7.  Checks finishing between 1-4 times this value are considered tolerated and above 4x is considered frustrating.

  Required: No

  Type: Number

## **status**
---
  The status of the newly created monitor.  Defaults to ENABLED.

  Required: No

  Type: String

  Valud Values: ENABLED, MUTED, DISABLED

## **treatRedirectAsFailure**
---
  Whether any redirect will result in a failure.  Defaults to False.  Valid only for SIMPLE monitors.

  Required: No

  Type: Boolean

## **type**
---
  Determines which type of synthetic monitor to create.

  Required: Yes

  Type: String

  Valid values: SIMPLE, BROWSER, SCRIPT_API, and SCRIPT_BROWSER

## **uri**
---
  The URI of the resource to ping.  Required for SIMPLE and BROWSER monitor types.

  Required: Only for SIMPLE and Browser types

  Type: String

## **ValidationString**
---
  A string expected to be returned in valid responses.  Valid only for SIMPLE and BROWSER monitors.

  Required: No

  Type: String

## **VerifySSL**
---
  Specifies whether each check will run a detailed OpenSSL handshake and alert on failures. Defaults to false.  Valid only for SIMPLE and BROWSER monitors.

  Required: No

  Type: Boolean

## Example

```yml
SyntheticMonitors:
  - name: Example1
    type: Simple
    frequency: 10
    uri: 'https://some.net'
    locations:
      - AWS_CA_CENTRAL_1
      - AWS_US_EAST_1
      - AWS_US_EAST_2
      - AWS_US_WEST_1
      - AWS_US_WEST_2
    slaThreshold: 7
    status: Enabled
    scriptVariables:
    -
      DOMAIN_NAME: some.net
      HEALTH_TENANT_NAME: Health

```