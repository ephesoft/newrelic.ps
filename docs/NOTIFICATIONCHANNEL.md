# NotificationChannels
```yml
NotificationChannels:
  name: String
  type: String
  configuration: NotificationChannelConfiguration
```
## **configuration**
---
  The configuration object used to define the notification channel.  Each notification policy type has a specific set of configurations that can be applied.

  Required: Yes

  Type: [NotificationChannelConfiguration](NOTIFICATIONCHANNELCONFIGURATION.md)

## **name**
---
  The name given to the defined notification channel.

  Required: Yes

  Type: String

## **type**
---
  The type of notification channel to be created.  Available types can be seen via the [New Relic API Explorer Create Channels](https://rpm.newrelic.com/api/explore/alerts_channels/create) page.

  Required: Yes

  Type: String

## Example
```yml
NotificationChannels:
  - name: OpsGenie-Sandbox-HighUrgency
    type: OpsGenie
    configuration:
      api_key: ${MyOpsGenieKey}
      tags:
        - Sandbox
        - HighUrgency
  - name: OpsGenie-Sandbox-MediumUrgency
    type: OpsGenie
    configuration:
      api_key: ${MyOpsGenieKey}
      tags:
        - Sandbox
        - MediumUrgency
```

