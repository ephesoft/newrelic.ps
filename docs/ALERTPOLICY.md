# AlertPolicies
```yml
AlertPolicies:
  name: String
  incident_preference: String
  channels: List
  conditions:
    - ConditionConfiguration
```
## **channels**
---
  A list of the notification channel names associated to this policy.  The channels listed here are used to send notifications when condition thresholds are crossed.

  Required: No

  Type: List

## **conditions**
---
  A list of condition configurations associated to this policy.  See the ConditionConfiguration section for details on how to describe conditions.

  Required: No

  Type: [ConditionConfiguration](CONDITION.md)

## **incident_preference
---
  Determines how New Relic Alerts will create incidents and group violations. More details are available via [New Relic Docs](https://docs.newrelic.com/docs/alerts/new-relic-alerts-beta/reviewing-alert-incidents/specify-when-new-relic-creates-incidents)

  Required: Yes

  Type: String

  Valid values: PER_POLICY, PER_CONDITION, PER_CONDITION_AND_TARGET

## **name**
---
  The name given to the defined alert policy

  Required: Yes

  Type: String

## Example
```yml
AlertPolicies:
  - name: Sandbox-Critical-HighUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-HighUrgency
    conditions:
      - type: static
        name: test1
        terms:
          - priority: 'CRITICAL'
            threshold: 110
            thresholdDuration: 60
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
      - type: baseline
        name: baselinetest1
        terms:
          - priority: 'CRITICAL'
            threshold: 110
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
  - name: Sandbox-Warning-MediumUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-MediumUrgency
```
