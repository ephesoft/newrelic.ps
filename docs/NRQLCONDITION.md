# NRQLCondition
Defines a NRQL condition on an alert policy.  NRQL conditions can be of three types: Baseline, Outlier, or Static.  Some properties are type-specific such as BaselineDirection which is only valid on baseline conditions.

NRQL Condition:
```yml
  conditions:
    - baselineDirection: String
      description: String
      enabled: Boolean
      expectedGroups: Number
      expiration: ConditionExpiration
      name: String
      nrql: ConditionNRQL
      openViolationOnGroupOverlap: Boolean
      runbookUrl: String
      terms: NRQLConditionTerms
      type: String
      valueFunction: String
      violationTimeLimit: Number
```

## **baselineDirection**
---
  Determines whether the condition is evaluated on the upper limit, lower limit, or both.  This property is only valid on baseline condition types.

  Required: No

  Type: String

  Valid values: UPPER_ONLY,LOWER_ONLY,UPPER_AND_LOWER

## **description**
---
  A text description for the condition

  Required: No

  Type: String

## **enabled**
---
  Specifies whether the condition should be created as enabled.  The condition defaults to enabled if not provided.

  Required: No

  Type: Boolean

## **expectedGroups**
---
  Specifies the number of expected groupings of returns values from an Outlier condition. This property is only valid on outlier condition types.

  Required: No

  Type: Number

## **expiration**
---
  This object defines how New Relic handles signal loss. Not valid on Outlier conditions.

  Required: No

  Type: [ConditionExpiration](NRQLCONDITIONEXPIRATION.md)

## **name**
---
  The name given to the defined condition.

  Required: Yes

  Type: String

## **nrql##
---
  This object defines the NRQL query properties for the condition.

  Required: Yes

  Type: [ConditionNRQL](NRQLCONDITIONNRQL.md)


## **openViolationsOnGroupOverlap**
---
  Specifies the number of expected groupings of returns values from an Outlier condition.  This property is only valid on outlier condition types.

  Required: No

  Type: Boolean

## **runbookUrl**
---
  Specifies a URL to the runbook for the condition.

  Required: No

  Type: String

## **terms**
---
  Terms define the properties around the condition threshold. A condition must have a term with a critical priority defined.  An additional term with a warning priority may also be defined and is optional.

  Required: Yes

  Type: [ConditionTerms](NRQLCONDITIONTERMS.md)

## **type**
---
  Determines the type of condition to be created.  Valid options are:
  * Baseline
  * Outlier
  * Static

  Required: Yes

  Type: String

## **valueFunction**
---
  Determines whether the condition is evaluated based on each query's returned value(SINGLE_VALUE) or on the sum of each query's returned values over the specified duration (SUM).  Defaults to SINGLE_VALUE if not provided.
  Valid options are:
  * SINGLE_VALUE
  * SUM

  Required: No

  Type: String

## **ViolationTimeLimit**
---
  Use to automatically close instance-based violations after the specified period.  Valid values are 'ONE_HOUR','TWO_HOURS','FOUR_HOURS','EIGHT_HOURS','TWELVE_HOURS','TWENTY_FOUR_HOURS'.

  Required: No

  Type: String

## Example

```yml
AlertPolicies:
  - name: Sandbox-Critical-HighUrgency
    incident_preference: PER_CONDITION_AND_TARGET
    channels:
      - OpsGenie-Sandbox-HighUrgency
    conditions:
      - type: static
        name: test1
        terms:
          - priority: 'CRITICAL'
            threshold: 120
            thresholdDuration: 60
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
      - type: baseline
        name: baselinetest1
        terms:
          - priority: 'CRITICAL'
            threshold: 110
        nrql:
          query: SELECT count(*) FROM AwsLambdaInvocation
```