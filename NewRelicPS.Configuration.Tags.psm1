Using module .\NewRelicPS.GraphQLQueries.psm1

<#
.Synopsis
    Create a new tag for the provided entity guid
.Description
    Create a new tag for the provided entity guid
.Example
    New-NRResourceTag -EntityGUID 'CPc5ODE1NHxTEE18QVBQTElDQVRJT058NTMwODQ4MTM3' -TagKey 'clientDomainName' -TagValue 'test.ephesoft.cloud' -APIKey 'fake-api-key'
.Example
    @('CPc5ODE1NHxTEE18QVBQTElDQVRJT058NTMwODQ4MTM3','CPc5ODE1NHxTEE18QVBQTElDQVRJT058NT985634') | New-NRResourceTag -Key 'clientDomainName' -Value 'test.ephesoft.cloud' -APIKey 'fake-api-key'
.Parameter EntityGUID
    Unique GUID which describes an entity in new relic
.Parameter TagKey
    Name of the tag to be created
.Parameter TagValues
    Value for the key
#>
Function New-NRResourceTag {
    [CMDLetBinding(SupportsShouldProcess = $true)]
    [CMDLetBinding()]
    Param (
        [Parameter (Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
        [string] $EntityGUID,
        [Parameter (Mandatory = $true)]
        [string] $TagKey,
        [Parameter (Mandatory = $true)]
        [string] $TagValue,
        [Parameter (Mandatory = $true)]
        [string] $APIKey
    )
    Begin {
        $headers = @{
            'X-Api-Key' = $APIKey
        }
        $url = 'https://api.newrelic.com/graphql'
    }
    Process {
        $graphqlQuery = Get-GraphQLQueryAddTagToEntity -EntityGUID $EntityGUID -TagKey $TagKey -TagValue $TagValue
        $body = @{
            query = $graphqlQuery
        } | ConvertTo-Json
        if ($PSCmdlet.ShouldProcess($EntityGUID, 'Create Tag ')) {
            $result = Invoke-RestMethod -Uri $url -headers $headers -body $body -Method 'Post' -ContentType 'application/json'
            return $result
        }
    }
}